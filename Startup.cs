﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using PippinPuffin.Persistence;
using Microsoft.EntityFrameworkCore;
using PippinPuffin.Core;
using Microsoft.Extensions.FileProviders;
using PippinPuffin.Services;
using Microsoft.AspNetCore.ResponseCompression;

namespace PippinPuffin
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
                builder = builder.AddUserSecrets<Startup>();

            builder = builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IThemeRepository, ThemeRepository>();
            services.AddScoped<ITemplateRepository, TemplateRepository>();
            services.AddScoped<IComponentSectionRepository, ComponentSectionRepository>();
            services.AddScoped<IComponentLayoutRepository, ComponentLayoutRepository>();
            services.AddScoped<IComponentColumnRepository, ComponentColumnRepository>();
            services.AddScoped<IComponentContentRepository, ComponentContentRepository>();
            services.AddScoped<IContentTextRepository, ContentTextRepository>();
            services.AddScoped<IContentButtonRepository, ContentButtonRepository>();
            services.AddScoped<IContentImageRepository, ContentImageRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ViewRender, ViewRender>();

            services.AddAutoMapper();
            services.AddDbContext<PippinPuffinDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Default"))
            );

            services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Optimal);
            services.AddResponseCompression();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseResponseCompression();
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), @"Contents")),
                RequestPath = new PathString("/Contents")
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
