using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Controllers.Api
{
    [Route("api/[controller]")]
    public class SectionController : Controller
    {
        private readonly IComponentSectionRepository repository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        private readonly ITemplateRepository templateRepository;
        public SectionController(
            IComponentSectionRepository repository,
            ITemplateRepository templateRepository,
            IMapper mapper,
            IUnitOfWork unitOfWork
        )
        {
            this.templateRepository = templateRepository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.repository = repository;
        }

        [HttpGet("template/{id}")]
        public async Task<IActionResult> GetByTemplateId(int id)
        {
            var sections = await repository.GetAllByTemplateIdAsync(id);

            if (sections == null)
                return NotFound();

            var resource = mapper.Map<List<ComponentSection>, List<ComponentSectionResource>>(sections);
            return Ok(resource);
        }

        [HttpPost("add/{templateId}")]
        public async Task<IActionResult> Add(int templateId, [FromBody] AddComponentSectionResource resource)
        {
            var template = await templateRepository.GetAsync(templateId);

            if (template == null)
                return NotFound();

            var section = mapper.Map<ComponentSectionResource, ComponentSection>(
                new ComponentSectionResource
                {
                    TemplateId = templateId,
                    Position = resource.Position,
                    LastUpdate = DateTime.Now
                }
             );
            repository.Add(section);
            template.LastUpdate = DateTime.Now;
            await unitOfWork.CompleteAsync();

            var sections = await repository.GetAllByTemplateIdAsync(templateId);
            ArrayList sectionIds = new ArrayList();

            foreach (var s in sections)
            {
                sectionIds.Add(s.Id);
            }

            sectionIds.Remove(section.Id);
            sectionIds.Insert(resource.Position, section.Id);

            sections.ForEach(cSection =>
                cSection.Position = sectionIds.IndexOf(cSection.Id)
            );

            await unitOfWork.CompleteAsync();

            var mapped = mapper.Map<ComponentSection, ComponentSectionResource>(section);


            return Ok(new
            {
                Section = mapped,
                Ids = sectionIds
            });
        }

        [HttpPost("sort/{templateId}")]
        public async Task<IActionResult> Sort(int templateId, [FromBody] int[] sectionIds)
        {
            var sections = await repository.GetAllByTemplateIdAsync(templateId);

            if (sections == null)
                return NotFound();

            sections.ForEach(cSection =>
                cSection.Position = Array.FindIndex(sectionIds, sortId =>
                    sortId.Equals(cSection.Id)
                )
            );

            await unitOfWork.CompleteAsync();

            return Ok(sectionIds);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var section = await repository.GetAsync(id);

            if (section == null)
                return NotFound();

            repository.Remove(section);
            await unitOfWork.CompleteAsync();

            var resource = mapper.Map<ComponentSection, ComponentSectionResource>(section);

            return Ok(resource);
        }
    }
}