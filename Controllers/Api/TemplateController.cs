using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Controllers.Api
{
    [Route("api/[controller]")]
    public class TemplateController : Controller
    {
        private readonly ITemplateRepository repository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public TemplateController(ITemplateRepository repository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.repository = repository;
        }

        [HttpGet]
        public async Task<IEnumerable<TemplateResource>> GetAll()
        {
            var templates = await repository.GetAllAsync();
            var templateResource = mapper.Map<List<Template>, List<TemplateResource>>(templates);
            return templateResource;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var template = await repository.GetAsync(id);

            if (template == null)
                return NotFound();

            var resource = mapper.Map<Template, TemplateResource>(template);
            return Ok(resource);
        }

        [HttpGet("theme/{id}")]
        public async Task<IActionResult> GetByThemeId(int id)
        {
            var template = await repository.GetByThemeIdAsync(id);

            if (template == null)
                return NotFound();

            var resource = mapper.Map<List<Template>, List<TemplateResource>>(template);
            return Ok(resource);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveTemplateResource saveTemplateResource)
        {
            var template = mapper.Map<SaveTemplateResource, Template>(saveTemplateResource);
            template.LastUpdate = DateTime.Now;

            repository.Add(template);
            await unitOfWork.CompleteAsync();

            var templateResource = mapper.Map<Template, TemplateResource>(template);

            return Ok(templateResource);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] SaveTemplateResource resource)
        {
            var template = await repository.GetAsync(id);

            if (template == null)
                return NotFound();

            var mapped = mapper.Map<SaveTemplateResource, Template>(resource, template);
            mapped.LastUpdate = DateTime.Now;

            await unitOfWork.CompleteAsync();

            var templateResource = mapper.Map<Template, TemplateResource>(mapped);

            return Ok(templateResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var template = await repository.GetAsync(id);

            if (template == null)
                return NotFound();

            repository.Remove(template);
            await unitOfWork.CompleteAsync();

            return Ok(template);
        }
    }
}
