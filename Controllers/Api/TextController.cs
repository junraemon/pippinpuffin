using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Controllers.Api
{
    [Route("api/[controller]")]
    public class TextController : Controller
    {
        private readonly IContentTextRepository textRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly IComponentContentRepository contentRepository;
        public TextController(
            IContentTextRepository textRepository,
            IComponentContentRepository contentRepository,
            IUnitOfWork unitOfWork, IMapper mapper
        )
        {
            this.contentRepository = contentRepository;
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.textRepository = textRepository;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] ContentTextResource resource)
        {
            var text = await textRepository.GetAsync(id);

            if (text == null)
                return NotFound();

            var mapped = mapper.Map<ContentTextResource, ContentText>(resource, text);
            mapped.LastUpdate = DateTime.Now;

            await unitOfWork.CompleteAsync();

            var content = await contentRepository.GetByContentTypeAsync(mapped.Id, ContentTypes.Text);

            return Ok(new
            {
                Content = content,
                Data = mapped
            });
        }
    }
}