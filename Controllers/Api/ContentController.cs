using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Controllers.Api
{
    [Route("api/[controller]")]
    public class ContentController : Controller
    {
        private readonly IComponentContentRepository contentRepository;
        private readonly IComponentColumnRepository columnRepository;
        private readonly IContentTextRepository textRepository;
        private readonly IContentButtonRepository buttonRepository;
        private readonly IContentImageRepository imageRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;

        public ContentController(
            IComponentContentRepository contentRepository,
            IComponentColumnRepository columnRepository,
            IContentTextRepository textRepository,
            IContentButtonRepository buttonRepository,
            IContentImageRepository imageRepository,
            IMapper mapper,
            IUnitOfWork unitOfWork
        )
        {
            this.contentRepository = contentRepository;
            this.columnRepository = columnRepository;
            this.textRepository = textRepository;
            this.buttonRepository = buttonRepository;
            this.imageRepository = imageRepository;
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        [HttpGet("column/{id}")]
        public async Task<IActionResult> GetByColumnId(int id)
        {
            var column = await columnRepository.GetAsync(id);

            if (column == null)
                return NotFound();

            var contents = await contentRepository.GetAllByColumnIdAsync(column.Id);
            var mapped = mapper.Map<List<ComponentContent>, List<ComponentContentResource>>(contents);

            var data = new ArrayList();
            foreach (var m in mapped)
            {
                dynamic contentData = "";

                if (m.ContentType == ContentTypes.Text)
                {
                    contentData = await textRepository.GetAsync(m.ContentTypeId);
                }
                else if (m.ContentType == ContentTypes.Button)
                {
                    contentData = await buttonRepository.GetAsync(m.ContentTypeId);
                }
                else if (m.ContentType == ContentTypes.Image)
                {
                    contentData = await imageRepository.GetAsync(m.ContentTypeId);
                }

                data.Add(new
                {
                    Content = m,
                    Data = contentData
                });
            }

            return Ok(data);
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] AddComponentContentResource resource)
        {
            var column = await columnRepository.GetAsync(resource.ColumnId);
            dynamic contentData = "";

            if (column == null)
                return NotFound();

            var contents = await contentRepository.GetAllByColumnIdAsync(resource.ColumnId);
            ArrayList contentIds = new ArrayList();

            foreach (var s in contents)
            {
                contentIds.Add(s.Id);
            }

            var content = contentRepository.CreateOrGetAsync(resource);

            if (resource.Type == ContentTypes.Text)
            {
                contentData = await textRepository.CreateOrGetAsync(resource.TypeId);
                content.ContentTypeId = contentData.Id;
            }
            else if (resource.Type == ContentTypes.Button)
            {
                contentData = await buttonRepository.CreateOrGetAsync(resource.TypeId);
                content.ContentTypeId = contentData.Id;
            }
            else if (resource.Type == ContentTypes.Image)
            {
                contentData = await imageRepository.CreateOrGetAsync(resource.TypeId);
                content.ContentTypeId = contentData.Id;
            }

            content.ContentType = resource.Type;
            contentRepository.Add(content);
            await unitOfWork.CompleteAsync();

            contentIds.Insert(resource.Position, content.Id);

            contents.ForEach(cContent =>
                cContent.Position = contentIds.IndexOf(cContent.Id)
            );

            await unitOfWork.CompleteAsync();

            var mapped = mapper.Map<ComponentContent, ComponentContentResource>(content);

            var payload = new
            {
                Content = mapped,
                Ids = contentIds,
                Data = contentData
            };

            return Ok(payload);
        }

        [HttpPost("sort")]
        public async Task<IActionResult> Sort([FromBody] SortComponentContentResource resource)
        {
            var contents = await contentRepository.GetAllByColumnIdAsync(resource.ColumnId);

            if (contents == null)
                return NotFound();

            if (resource.MovedContentId != 0)
            {
                var movedContent = await contentRepository.GetAsync(resource.MovedContentId);
                movedContent.ComponentColumnId = resource.ColumnId;
                movedContent.LastUpdate = DateTime.Now;
            }

            contents.ForEach(content =>
                content.Position = Array.FindIndex(resource.ContentIds, sortId =>
                    sortId.Equals(content.Id)
                )
            );

            await unitOfWork.CompleteAsync();

            return Ok(resource);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] SaveComponentContentResource resource)
        {
            var content = await contentRepository.GetAsync(id);
            dynamic contentData = "";

            if (content == null)
                return NotFound();

            var mapped = mapper.Map<SaveComponentContentResource, ComponentContent>(resource, content);
            mapped.LastUpdate = DateTime.Now;

            await unitOfWork.CompleteAsync();

            if (content.ContentType == ContentTypes.Text)
            {
                contentData = await textRepository.GetAsync(content.ContentTypeId);
            }
            else if (content.ContentType == ContentTypes.Button)
            {
                contentData = await buttonRepository.GetAsync(content.ContentTypeId);
            }
            else if (content.ContentType == ContentTypes.Image)
            {
                contentData = await imageRepository.GetAsync(content.ContentTypeId);
            }

            return Ok(new
            {
                Content = mapped,
                Data = contentData
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var content = await contentRepository.GetAsync(id);

            if (content == null)
                return NotFound();

            if (content.ContentType == ContentTypes.Text)
            {
                var text = await textRepository.GetAsync(content.ContentTypeId);
                textRepository.Remove(text);
            }
            else if (content.ContentType == ContentTypes.Button)
            {
                var button = await buttonRepository.GetAsync(content.ContentTypeId);
                buttonRepository.Remove(button);
            }

            contentRepository.Remove(content);
            await unitOfWork.CompleteAsync();

            var mapped = mapper.Map<ComponentContent, ComponentContentResource>(content);

            return Ok(mapped);
        }
    }
}
