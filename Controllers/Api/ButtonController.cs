using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Controllers.Api
{
    [Route("api/[controller]")]
    public class ButtonController : Controller
    {
        private readonly IContentButtonRepository buttonRepository;
        private readonly IComponentContentRepository contentRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public ButtonController(
            IContentButtonRepository buttonRepository,
            IComponentContentRepository contentRepository,
            IUnitOfWork unitOfWork, IMapper mapper
        )
        {
            this.buttonRepository = buttonRepository;
            this.contentRepository = contentRepository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] ContentButtonResource resource)
        {
            var button = await buttonRepository.GetAsync(id);

            if (button == null)
                return NotFound();

            var mapped = mapper.Map<ContentButtonResource, ContentButton>(resource, button);
            mapped.LastUpdate = DateTime.Now;

            await unitOfWork.CompleteAsync();

            var content = await contentRepository.GetByContentTypeAsync(mapped.Id, ContentTypes.Button);

            return Ok(new
            {
                Content = content,
                Data = mapped
            });
        }
    }
}