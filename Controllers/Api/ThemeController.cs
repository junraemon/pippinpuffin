using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Controllers.Api
{
    [Route("api/[controller]")]
    public class ThemeController : Controller
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        private readonly IThemeRepository repository;

        public ThemeController(IThemeRepository repository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.repository = repository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<ViewThemesResource>> GetAll()
        {
            var themes = await repository.GetAllAsync();
            var themesResource = mapper.Map<List<Theme>, List<ViewThemesResource>>(themes);
            return themesResource;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var theme = await repository.GetAsync(id);

            if (theme == null)
                return NotFound();

            var themesResource = mapper.Map<Theme, ViewThemeResource>(theme);
            return Ok(themesResource);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SaveThemeResource saveThemeResource)
        {
            var mappedTheme = mapper.Map<SaveThemeResource, Theme>(saveThemeResource);
            mappedTheme.LastUpdate = DateTime.Now;

            repository.Add(mappedTheme);
            await unitOfWork.CompleteAsync();

            var getTheme = await repository.GetAsync(mappedTheme.Id);
            var themeResource = mapper.Map<Theme, ViewThemesResource>(getTheme);

            return Ok(themeResource);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] SaveThemeResource resource)
        {
            var theme = await repository.GetAsync(id);

            if (theme == null)
                return NotFound();

            var mappedTheme = mapper.Map<SaveThemeResource, Theme>(resource, theme);
            mappedTheme.LastUpdate = DateTime.Now;

            await unitOfWork.CompleteAsync();

            var themeResource = mapper.Map<Theme, ViewThemesResource>(mappedTheme);
            return Ok(themeResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var theme = await repository.GetAsync(id);

            if (theme == null)
                return NotFound();

            repository.Remove(theme);
            await unitOfWork.CompleteAsync();

            return Ok(id);
        }
    }
}