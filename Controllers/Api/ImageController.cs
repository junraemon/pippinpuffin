using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Controllers.Api
{
    [Route("api/[controller]")]
    public class ImageController : Controller
    {
        private readonly IContentImageRepository imageRepository;
        private readonly IComponentContentRepository contentRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public ImageController(
            IContentImageRepository imageRepository,
            IComponentContentRepository contentRepository,
            IUnitOfWork unitOfWork, IMapper mapper
        )
        {
            this.imageRepository = imageRepository;
            this.contentRepository = contentRepository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] ContentImageResource resource)
        {
            var image = await imageRepository.GetAsync(id);

            if (image == null)
                return NotFound();

            var mapped = mapper.Map<ContentImageResource, ContentImage>(resource, image);
            mapped.LastUpdate = DateTime.Now;

            await unitOfWork.CompleteAsync();

            var content = await contentRepository.GetByContentTypeAsync(mapped.Id, ContentTypes.Image);

            return Ok(new
            {
                Content = content,
                Data = mapped
            });
        }
    }
}