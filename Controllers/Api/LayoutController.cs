using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Controllers.Api
{
    [Route("api/[controller]")]
    public class LayoutController : Controller
    {
        private readonly IComponentLayoutRepository repository;
        private readonly IComponentSectionRepository sectionRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public LayoutController(
            IComponentLayoutRepository repository,
            IComponentSectionRepository sectionRepository,
            IMapper mapper,
            IUnitOfWork unitOfWork
        )
        {
            this.sectionRepository = sectionRepository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.repository = repository;
        }

        [HttpGet("section/{id}")]
        public async Task<IActionResult> GetBySectionId(int id)
        {
            var section = await sectionRepository.GetAsync(id);

            if (section == null)
                return NotFound();

            var layouts = await repository.GetAllBySectionIdAsync(id);

            var resource = mapper.Map<List<ComponentLayout>, List<ComponentLayoutResource>>(layouts);
            return Ok(resource);
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] AddComponentLayoutResource resource)
        {
            var section = await sectionRepository.GetAsync(resource.SectionId);

            if (section == null)
                return NotFound();

            var layouts = await repository.GetAllBySectionIdAsync(resource.SectionId);
            var layoutIds = await repository.GetIdsBySectionIdAsync(resource.SectionId);
            var layout = repository.CreateLayoutByResource(resource);

            repository.Add(layout);
            await unitOfWork.CompleteAsync();
            layoutIds.Insert(resource.Position, layout.Id);

            layouts.ForEach(cLayout =>
                cLayout.Position = layoutIds.IndexOf(cLayout.Id)
            );

            section.LastUpdate = DateTime.Now;
            await unitOfWork.CompleteAsync();

            var mapped = mapper.Map<ComponentLayout, ComponentLayoutResource>(layout);

            return Ok(new
            {
                Layout = mapped,
                Ids = layoutIds
            });
        }

        [HttpPost("sort")]
        public async Task<IActionResult> Sort([FromBody] SortComponentLayoutResource resource)
        {
            var layouts = await repository.GetAllBySectionIdAsync(resource.SectionId);

            if (layouts == null)
                return NotFound();

            if (resource.MovedLayoutId != 0)
            {
                var movedLayout = await repository.GetAsync(resource.MovedLayoutId);
                movedLayout.ComponentSectionId = resource.SectionId;
                movedLayout.LastUpdate = DateTime.Now;
            }

            layouts.ForEach(layout =>
                layout.Position = Array.FindIndex(resource.LayoutIds, sortId =>
                    sortId.Equals(layout.Id)
                )
            );

            await unitOfWork.CompleteAsync();

            return Ok(resource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var layout = await repository.GetAsync(id);

            if (layout == null)
                return NotFound();

            repository.Remove(layout);
            await unitOfWork.CompleteAsync();

            var mapped = mapper.Map<ComponentLayout, ComponentLayoutResource>(layout);

            return Ok(mapped);
        }
    }
}