using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using PippinPuffin.Core;
using PippinPuffin.Services;
using SharpScss;

namespace PippinPuffin.Controllers
{
    public class SassController : Controller
    {
        private readonly ViewRender viewRender;
        private readonly IThemeRepository repository;
        public SassController(IThemeRepository repository, ViewRender viewRender)
        {
            this.repository = repository;
            this.viewRender = viewRender;
        }

        public async Task<ContentResult> Render(int id)
        {
            var theme = await repository.GetAsync(id);

            string defaultCss = @"@import ""Contents/Styles/foundation-email-scss/settings"";";
            string mainSCSS = @"@import ""Contents/Styles/foundation-email-scss/app"";";

            var result = Scss.ConvertToCss(defaultCss + theme.CustomCss + mainSCSS, new ScssOptions()
            {
                OutputStyle = ScssOutputStyle.Compressed
            });

            return Content(result.Css, new MediaTypeHeaderValue("text/css"));
        }

        /*public async Task<IActionResult> Premail()
        {
            var sections = await context.ComponentSections.OrderBy(cSections => cSections.Position)
                .Include(cSections => cSections.ComponentLayouts)
                    .ThenInclude(cLayouts => cLayouts.ComponentColumns)
                        .ThenInclude(cColumns => cColumns.ComponentContents).ToListAsync();

            string htmlSource = viewRender.Render("TemplatePage/Preview", sections);

            var result = PreMailer.Net.PreMailer.MoveCssInline(htmlSource, false, ignoreElements: "#ignore");

            return Content(result.Html, new MediaTypeHeaderValue("text/html"));
        }*/
    }
}