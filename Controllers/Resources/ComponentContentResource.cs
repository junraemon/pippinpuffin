using System;

namespace PippinPuffin.Controllers.Resources
{
    public class ComponentContentResource
    {
        public int Id { get; set; }
        public int Position { get; set; }
        public string ContentType { get; set; }
        public int ContentTypeId { get; set; }
        public int ComponentColumnId { get; set; }
        public string BackgroundColor { get; set; }
        public int Spacing { get; set; }
        public string CustomClass { get; set; }
        public int CalloutStyle { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}