using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Controllers.Resources
{
    public class ComponentLayoutResource
    {
        public int Id { get; set; }
        public int Position { get; set; }
        public int ComponentSectionId { get; set; }
        public DateTime LastUpdate { get; set; }
        public ICollection<ComponentColumnResource> ComponentColumns { get; set; }
        public ComponentLayoutResource()
        {
            ComponentColumns = new Collection<ComponentColumnResource>();
        }
    }
}