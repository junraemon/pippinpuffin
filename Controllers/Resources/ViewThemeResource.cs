using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PippinPuffin.Controllers.Resources
{
    public class ViewThemeResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CustomCss { get; set; }
        public DateTime LastUpdate { get; set; }
        public ICollection<TemplateResource> Templates { get; set; }
        public ViewThemeResource()
        {
            Templates = new Collection<TemplateResource>();
        }
    }
}