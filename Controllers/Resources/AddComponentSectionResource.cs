using System;

namespace PippinPuffin.Controllers.Resources
{
    public class AddComponentSectionResource
    {
        public int Type { get; set; }
        public int Position { get; set; }
    }
}