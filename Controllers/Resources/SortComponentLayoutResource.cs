namespace PippinPuffin.Controllers.Resources
{
    public class SortComponentLayoutResource
    {
        public int SectionId { get; set; }
        public int MovedLayoutId { get; set; }
        public int[] LayoutIds { get; set; }
    }
}