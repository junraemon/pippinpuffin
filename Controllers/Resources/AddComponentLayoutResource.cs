namespace PippinPuffin.Controllers.Resources
{
    public class AddComponentLayoutResource
    {
        public int SectionId { get; set; }
        public int LayoutId { get; set; }
        public int NumberOfColumns { get; set; }
        public int Position { get; set; }
    }
}