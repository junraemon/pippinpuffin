namespace PippinPuffin.Controllers.Resources
{
    public class ContentButtonResource
    {
        public string Link { get; set; }
        public string Caption { get; set; }
        public string Alignment { get; set; }
        public string Size { get; set; }
        public string Type { get; set; }
        public string Corners { get; set; }
        public string Expanded { get; set; }
    }
}