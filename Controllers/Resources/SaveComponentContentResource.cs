namespace PippinPuffin.Controllers.Resources
{
    public class SaveComponentContentResource
    {
        public string BackgroundColor { get; set; }
        public int Spacing { get; set; }
        public string CustomClass { get; set; }
        public int CalloutStyle { get; set; }
    }
}
