using System;

namespace PippinPuffin.Controllers.Resources
{
    public class ComponentSectionResource
    {
        public int Id { get; set; }
        public int Position { get; set; }
        public int TemplateId { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}