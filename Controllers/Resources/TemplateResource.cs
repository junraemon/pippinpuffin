using System;

namespace PippinPuffin.Controllers.Resources
{
    public class TemplateResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ThemeId { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
