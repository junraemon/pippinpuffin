using System;

namespace PippinPuffin.Controllers.Resources
{
    public class ViewThemesResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CustomCss { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}