namespace PippinPuffin.Controllers.Resources
{
    public class SaveTemplateResource
    {
        public string Name { get; set; }
        public int ThemeId { get; set; }
    }
}
