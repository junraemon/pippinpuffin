namespace PippinPuffin.Controllers.Resources
{
    public class ContentImageResource
    {
        public string Url { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Alignment { get; set; }
    }
}