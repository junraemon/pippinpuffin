namespace PippinPuffin.Controllers.Resources
{
    public class ComponentColumnResource
    {
        public int Id { get; set; }
        public string ColumnSizes { get; set; }
        public int ComponentLayoutId { get; set; }
    }
}