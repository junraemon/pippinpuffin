namespace PippinPuffin.Controllers.Resources
{
    public class SaveThemeResource
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string CustomCss { get; set; }
    }
}