namespace PippinPuffin.Controllers.Resources
{
    public class ContentTextResource
    {
        public string Content { get; set; }
    }
}