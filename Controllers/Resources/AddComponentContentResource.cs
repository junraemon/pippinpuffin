using PippinPuffin.Core.Models;

namespace PippinPuffin.Controllers.Resources
{
    public class AddComponentContentResource
    {
        public int ColumnId { get; set; }
        public string Type { get; set; }
        public int TypeId { get; set; }
        public int Position { get; set; }
    }
}