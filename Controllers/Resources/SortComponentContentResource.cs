namespace PippinPuffin.Controllers.Resources
{
    public class SortComponentContentResource
    {
        public int ColumnId { get; set; }
        public int MovedContentId { get; set; }
        public int[] ContentIds { get; set; }
    }
}