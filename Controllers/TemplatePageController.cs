using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PippinPuffin.Core;
using PippinPuffin.Persistence;

namespace PippinPuffin.Controllers
{
    public class TemplatePageController : Controller
    {
        private readonly ITemplateRepository repository;
        private readonly PippinPuffinDbContext context;
        public TemplatePageController(PippinPuffinDbContext context, ITemplateRepository repository)
        {
            this.context = context;
            this.repository = repository;
        }
        public async Task<IActionResult> Edit(int id)
        {
            var template = await context.Templates.SingleOrDefaultAsync(t => t.Id == id);

            if (template == null)
                return NotFound();

            return View(template);
        }

        public async Task<IActionResult> Preview(int id)
        {
            var theme = await context.Themes.SingleOrDefaultAsync(t => t.Id == id);

            if (theme == null)
                return NotFound();

            return View(theme.Id);
        }
    }
}