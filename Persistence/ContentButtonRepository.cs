using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Persistence
{
    public class ContentButtonRepository : IContentButtonRepository
    {
        private readonly PippinPuffinDbContext context;
        private readonly IUnitOfWork unitOfWork;
        public ContentButtonRepository(PippinPuffinDbContext context, IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.context = context;
        }
        public void Add(ContentButton contentButton)
        {
            context.ContentButtons.Add(contentButton);
        }

        public async Task<ContentButton> CreateOrGetAsync(int id)
        {
            var button = await context.ContentButtons.SingleOrDefaultAsync(c => c.Id == id);

            if (button == null)
            {
                button = new ContentButton
                {
                    Link = "",
                    LastUpdate = DateTime.Now
                };
            }

            Add(button);
            await unitOfWork.CompleteAsync();

            return button;
        }

        public async Task<ContentButton> GetAsync(int id)
        {
            return await context.ContentButtons.SingleOrDefaultAsync(c => c.Id == id);
        }

        public void Remove(ContentButton contentButton)
        {
            context.ContentButtons.Remove(contentButton);
        }
    }
}