using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Persistence
{
    public class ThemeRepository : IThemeRepository
    {
        private readonly PippinPuffinDbContext context;
        public ThemeRepository(PippinPuffinDbContext context)
        {
            this.context = context;
        }

        public async Task<List<Theme>> GetAllAsync()
        {
            return await context.Themes.ToListAsync();
        }
        public async Task<Theme> GetAsync(int id)
        {
            return await context.Themes.Include(t => t.Templates).SingleOrDefaultAsync(c => c.Id == id);
        }
        public void Add(Theme theme)
        {
            context.Themes.Add(theme);
        }
        public void Remove(Theme theme)
        {
            context.Themes.Remove(theme);
        }
    }
}