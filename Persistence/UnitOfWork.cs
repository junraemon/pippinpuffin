using System.Threading.Tasks;
using PippinPuffin.Core;

namespace PippinPuffin.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PippinPuffinDbContext context;
        public UnitOfWork(PippinPuffinDbContext context)
        {
            this.context = context;
        }
        public async Task CompleteAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}