using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Persistence
{
    public class TemplateRepository : ITemplateRepository
    {
        private readonly PippinPuffinDbContext context;
        public TemplateRepository(PippinPuffinDbContext context)
        {
            this.context = context;
        }
        public async Task<List<Template>> GetAllAsync()
        {
            return await context.Templates.ToListAsync();
        }
        public async Task<Template> GetAsync(int id)
        {
            return await context.Templates.SingleOrDefaultAsync(c => c.Id == id);
        }
        public async Task<List<Template>> GetByThemeIdAsync(int id)
        {
            return await context.Templates.Where(t => t.ThemeId == id).ToListAsync();
        }
        public void Add(Template template)
        {
            context.Templates.Add(template);
        }
        public void Remove(Template template)
        {
            context.Templates.Remove(template);
        }
    }
}
