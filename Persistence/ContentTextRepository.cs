using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Persistence
{
    public class ContentTextRepository : IContentTextRepository
    {
        private readonly PippinPuffinDbContext context;
        private readonly IUnitOfWork unitOfWork;
        public ContentTextRepository(PippinPuffinDbContext context, IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.context = context;
        }
        public async Task<ContentText> GetAsync(int id)
        {
            return await context.ContentTexts.SingleOrDefaultAsync(c => c.Id == id);
        }
        public void Add(ContentText contentText)
        {
            context.ContentTexts.Add(contentText);
        }
        public void Remove(ContentText contentText)
        {
            context.ContentTexts.Remove(contentText);
        }
        public async Task<ContentText> CreateOrGetAsync(int id)
        {
            var text = await context.ContentTexts.SingleOrDefaultAsync(c => c.Id == id);

            if (text == null)
            {
                text = new ContentText
                {
                    Content = "",
                    LastUpdate = DateTime.Now
                };
            }

            this.Add(text);
            await unitOfWork.CompleteAsync();

            return text;
        }
    }
}