using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Persistence
{
    public class ComponentSectionRepository : IComponentSectionRepository
    {
        private readonly PippinPuffinDbContext context;
        public ComponentSectionRepository(PippinPuffinDbContext context)
        {
            this.context = context;
        }
        public async Task<List<ComponentSection>> GetAllByTemplateIdAsync(int id)
        {
            return await context.ComponentSections
                .Where(c => c.TemplateId == id)
                .OrderBy(c => c.Position)
                .ToListAsync();
        }
        public async Task<ComponentSection> GetAsync(int id)
        {
            return await context.ComponentSections.SingleOrDefaultAsync(c => c.Id == id);
        }
        public void Add(ComponentSection componentSection)
        {
            context.ComponentSections.Add(componentSection);
        }
        public void Remove(ComponentSection componentSection)
        {
            context.ComponentSections.Remove(componentSection);
        }
    }
}