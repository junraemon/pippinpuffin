using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Persistence
{
    public class ComponentContentRepository : IComponentContentRepository
    {
        private readonly PippinPuffinDbContext context;
        public ComponentContentRepository(PippinPuffinDbContext context)
        {
            this.context = context;
        }
        public async Task<List<ComponentContent>> GetAllByColumnIdAsync(int id)
        {
            return await context.ComponentContents
                .Where(content => content.ComponentColumnId == id)
                .OrderBy(content => content.Position)
                .ToListAsync();
        }
        public async Task<ComponentContent> GetAsync(int id)
        {
            return await context.ComponentContents.SingleOrDefaultAsync(content => content.Id == id);
        }
        public void Add(ComponentContent componentContent)
        {
            context.ComponentContents.Add(componentContent);
        }
        public void Remove(ComponentContent componentContent)
        {
            context.ComponentContents.Remove(componentContent);
        }
        public ComponentContent CreateOrGetAsync(AddComponentContentResource resource)
        {
            return new ComponentContent
            {
                ComponentColumnId = resource.ColumnId,
                Position = resource.Position,
                LastUpdate = DateTime.Now,
            };
        }

        public async Task<ComponentContent> GetByContentTypeAsync(int id, string type)
        {
            return await context.ComponentContents
                .Where(content => content.ContentType == type)
                .Where(content => content.ContentTypeId == id)
                .SingleOrDefaultAsync();
        }
    }
}