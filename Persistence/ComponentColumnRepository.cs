using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Persistence
{
    public class ComponentColumnRepository : IComponentColumnRepository
    {
        private readonly PippinPuffinDbContext context;
        public ComponentColumnRepository(PippinPuffinDbContext context)
        {
            this.context = context;
        }

        public async Task<ComponentColumn> GetAsync(int id)
        {
            return await context.ComponentColumns.SingleOrDefaultAsync(column => column.Id == id);
        }
    }
}