using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Persistence
{
    public class ContentImageRepository : IContentImageRepository
    {
        private readonly PippinPuffinDbContext context;
        private readonly IUnitOfWork unitOfWork;
        public ContentImageRepository(PippinPuffinDbContext context, IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.context = context;
        }

        public void Add(ContentImage contentImage)
        {
            context.ContentImages.Add(contentImage);
        }

        public async Task<ContentImage> CreateOrGetAsync(int id)
        {
            var image = await context.ContentImages.SingleOrDefaultAsync(c => c.Id == id);

            if (image == null)
            {
                image = new ContentImage
                {
                    LastUpdate = DateTime.Now
                };
            }

            Add(image);
            await unitOfWork.CompleteAsync();

            return image;
        }

        public async Task<ContentImage> GetAsync(int id)
        {
            return await context.ContentImages.SingleOrDefaultAsync(c => c.Id == id);
        }

        public void Remove(ContentImage contentImage)
        {
            context.ContentImages.Remove(contentImage);
        }
    }
}