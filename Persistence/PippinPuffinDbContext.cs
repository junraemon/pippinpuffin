using Microsoft.EntityFrameworkCore;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Persistence
{
    public class PippinPuffinDbContext : DbContext
    {
        public DbSet<Theme> Themes { get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<ComponentSection> ComponentSections { get; set; }
        public DbSet<ComponentLayout> ComponentLayouts { get; set; }
        public DbSet<ComponentColumn> ComponentColumns { get; set; }
        public DbSet<ComponentContent> ComponentContents { get; set; }
        public DbSet<ContentText> ContentTexts { get; set; }
        public DbSet<ContentButton> ContentButtons { get; set; }
        public DbSet<ContentImage> ContentImages { get; set; }
        public PippinPuffinDbContext(DbContextOptions<PippinPuffinDbContext> options)
          : base(options)
        {
        }
    }
}
