using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Persistence
{
    public class ComponentLayoutRepository : IComponentLayoutRepository
    {
        private readonly PippinPuffinDbContext context;
        public ComponentLayoutRepository(PippinPuffinDbContext context)
        {
            this.context = context;
        }
        public void Add(ComponentLayout componentLayout)
        {
            context.ComponentLayouts.Add(componentLayout);
        }

        public async Task<List<ComponentLayout>> GetAllBySectionIdAsync(int id)
        {
            return await context.ComponentLayouts
                .Where(layout => layout.ComponentSectionId == id)
                .Include(layout => layout.ComponentColumns)
                .OrderBy(layout => layout.Position)
                .ToListAsync();
        }

        public async Task<ComponentLayout> GetAsync(int id)
        {
            return await context.ComponentLayouts.SingleOrDefaultAsync(layout => layout.Id == id);
        }

        public void Remove(ComponentLayout componentLayout)
        {
            context.ComponentLayouts.Remove(componentLayout);
        }

        public async Task<ArrayList> GetIdsBySectionIdAsync(int sectionId)
        {
            ArrayList layoutIds = new ArrayList();

            var layouts = await context.ComponentLayouts
                .Where(layout => layout.ComponentSectionId == sectionId)
                .Include(layout => layout.ComponentColumns)
                .OrderBy(layout => layout.Position)
                .ToListAsync();

            foreach (var s in layouts)
            {
                layoutIds.Add(s.Id);
            }

            return layoutIds;
        }

        public ComponentLayout CreateLayoutByResource(AddComponentLayoutResource resource)
        {
            var layout = new ComponentLayout();
            layout.LastUpdate = DateTime.Now;
            layout.ComponentSectionId = resource.SectionId;
            layout.Position = resource.Position;
            layout.ComponentColumns = new List<ComponentColumn>();

            for (int i = 1; i <= resource.NumberOfColumns; i++)
            {
                var column = new ComponentColumn();
                column.ColumnSizes = (12 / resource.NumberOfColumns).ToString();
                layout.ComponentColumns.Add(column);
            }

            return layout;
        }
    }
}