(function () {

  if (isRenderFromIframe()) {
    return false;
  }

  window.document.oncontextmenu = function () {
    return false;
  };

})();

function isRenderFromIframe() {
  return self === top;
}
