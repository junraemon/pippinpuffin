using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PippinPuffin.Core.Models
{
    public class ComponentLayout
    {
        public int Id { get; set; }
        public int Position { get; set; }
        public int ComponentSectionId { get; set; }
        public DateTime LastUpdate { get; set; }
        public ComponentSection ComponentSection { get; set; }
        public ICollection<ComponentColumn> ComponentColumns { get; set; }
        public ComponentLayout()
        {
            ComponentColumns = new Collection<ComponentColumn>();
        }
    }
}
