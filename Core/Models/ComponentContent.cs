using System;

namespace PippinPuffin.Core.Models
{
    public class ComponentContent
    {
        public int Id { get; set; }
        public int Position { get; set; }
        public int ComponentColumnId { get; set; }
        public ComponentColumn ComponentColumn { get; set; }
        public string BackgroundColor { get; set; }
        public int Spacing { get; set; }
        public string CustomClass { get; set; }
        public int CalloutStyle { get; set; }
        public string ContentType { get; set; }
        public int ContentTypeId { get; set; }
        public DateTime LastUpdate { get; set; }
    }

    public static class ContentTypes
    {
        public static string Text = "text";
        public static string Button = "button";
        public static string Image = "image";
    }
}
