using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PippinPuffin.Core.Models
{
    public class ComponentSection
    {
        public int Id { get; set; }
        public int Position { get; set; }
        public int TemplateId { get; set; }
        public Template Template { get; set; }
        public DateTime LastUpdate { get; set; }
        public ICollection<ComponentLayout> ComponentLayouts { get; set; }
        public ComponentSection()
        {
            ComponentLayouts = new Collection<ComponentLayout>();
        }
    }
}
