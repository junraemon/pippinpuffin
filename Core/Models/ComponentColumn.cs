using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PippinPuffin.Core.Models
{
    public class ComponentColumn
    {
        public int Id { get; set; }
        public string ColumnSizes { get; set; }
        public int ComponentLayoutId { get; set; }
        public ComponentLayout ComponentLayout { get; set; }
        public ICollection<ComponentContent> ComponentContents { get; set; }
        public ComponentColumn()
        {
            ComponentContents = new Collection<ComponentContent>();
        }
    }
}
