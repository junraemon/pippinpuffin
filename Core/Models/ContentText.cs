using System;

namespace PippinPuffin.Core.Models
{
    public class ContentText
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}