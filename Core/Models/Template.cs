using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Core.Models
{
    public class Template
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int ThemeId { get; set; }
        public Theme Theme { get; set; }
        public DateTime LastUpdate { get; set; }
        public ICollection<ComponentSection> ComponentSections { get; set; }

        public Template()
        {
            ComponentSections = new Collection<ComponentSection>();
        }
    }
}
