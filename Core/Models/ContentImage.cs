using System;

namespace PippinPuffin.Core.Models
{
    public class ContentImage
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Alignment { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}