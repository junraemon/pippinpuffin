using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace PippinPuffin.Core.Models
{
    public class Theme
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public string Description { get; set; }
        public string CustomCss { get; set; }
        public DateTime LastUpdate { get; set; }
        public ICollection<Template> Templates { get; set; }
        public Theme()
        {
            Templates = new Collection<Template>();
        }
    }
}