using System;

namespace PippinPuffin.Core.Models
{
    public class ContentButton
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public string Caption { get; set; }
        public string Alignment { get; set; }
        public string Size { get; set; }
        public string Type { get; set; }
        public string Corners { get; set; }
        public string Expanded { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}