using System.Threading.Tasks;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Core
{
    public interface IComponentColumnRepository
    {
        Task<ComponentColumn> GetAsync(int id);
    }
}