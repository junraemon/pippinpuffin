using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Core
{
    public interface IComponentLayoutRepository
    {
        Task<List<ComponentLayout>> GetAllBySectionIdAsync(int id);
        Task<ComponentLayout> GetAsync(int id);
        void Add(ComponentLayout componentLayout);
        void Remove(ComponentLayout componentLayout);
        Task<ArrayList> GetIdsBySectionIdAsync(int sectionId);
        ComponentLayout CreateLayoutByResource(AddComponentLayoutResource resource);
    }
}