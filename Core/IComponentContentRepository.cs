using System.Collections.Generic;
using System.Threading.Tasks;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Core
{
    public interface IComponentContentRepository
    {
        Task<List<ComponentContent>> GetAllByColumnIdAsync(int id);
        Task<ComponentContent> GetAsync(int id);
        Task<ComponentContent> GetByContentTypeAsync(int id, string type);
        void Add(ComponentContent componentContent);
        void Remove(ComponentContent componentContent);
        ComponentContent CreateOrGetAsync(AddComponentContentResource resource);
    }
}