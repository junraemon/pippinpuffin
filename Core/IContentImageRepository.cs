using System.Threading.Tasks;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Core
{
    public interface IContentImageRepository
    {
        Task<ContentImage> GetAsync(int id);
        Task<ContentImage> CreateOrGetAsync(int id);
        void Add(ContentImage contentImage);
        void Remove(ContentImage contentImage);
    }
}