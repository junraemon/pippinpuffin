using System.Collections.Generic;
using System.Threading.Tasks;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Core
{
    public interface IThemeRepository
    {
        Task<List<Theme>> GetAllAsync();
        Task<Theme> GetAsync(int id);
        void Add(Theme theme);
        void Remove(Theme theme);
    }
}