using System.Collections.Generic;
using System.Threading.Tasks;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Core
{
    public interface ITemplateRepository
    {
        Task<List<Template>> GetAllAsync();
        Task<Template> GetAsync(int id);
        Task<List<Template>> GetByThemeIdAsync(int id);
        void Add(Template template);
        void Remove(Template template);
    }
}
