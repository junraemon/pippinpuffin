using System.Threading.Tasks;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Core
{
    public interface IContentTextRepository
    {
        Task<ContentText> GetAsync(int id);
        Task<ContentText> CreateOrGetAsync(int id);
        void Add(ContentText contentText);
        void Remove(ContentText contentText);
    }
}