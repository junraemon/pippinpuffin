using System.Threading.Tasks;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Core
{
    public interface IContentButtonRepository
    {
        Task<ContentButton> GetAsync(int id);
        Task<ContentButton> CreateOrGetAsync(int id);
        void Add(ContentButton contentButton);
        void Remove(ContentButton contentButton);
    }
}