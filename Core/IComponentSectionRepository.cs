using System.Collections.Generic;
using System.Threading.Tasks;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Core
{
    public interface IComponentSectionRepository
    {
        Task<List<ComponentSection>> GetAllByTemplateIdAsync(int id);
        Task<ComponentSection> GetAsync(int id);
        void Add(ComponentSection componentSection);
        void Remove(ComponentSection componentSection);
    }
}