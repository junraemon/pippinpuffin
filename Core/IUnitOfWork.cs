using System.Threading.Tasks;

namespace PippinPuffin.Core
{
    public interface IUnitOfWork
    {
        Task CompleteAsync();
    }
}