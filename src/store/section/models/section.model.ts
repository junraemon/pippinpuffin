export interface TSection {
  position: number;
  templateId: number;
  id?: number;
  lastUpdate?: Date;
  isSelected?: boolean;
}
