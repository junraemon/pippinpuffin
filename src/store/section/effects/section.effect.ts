import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable ,  of } from "rxjs";

import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap, tap, switchMap } from "rxjs/operators";

import * as fromSection from "../actions";
import { SectionService } from "../services";
import { TSection } from "../models";

@Injectable()
export class SectionEffects {
  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType(fromSection.LOAD_SECTIONS),
    mergeMap((action: fromSection.LoadSections) =>
      this.sectionService
        .getByTemplateId(action.templateId)
        .pipe(
          map(data => new fromSection.LoadSectionsSuccess(data)),
          catchError(error => of(new fromSection.LoadSectionsFail(error)))
        )
    )
  );

  @Effect()
  add$: Observable<Action> = this.actions$.pipe(
    ofType(fromSection.ADD_SECTION),
    mergeMap((action: fromSection.AddSection) =>
      this.sectionService
        .add(action.templateId, action.newSection)
        .pipe(
          mergeMap(data => [
            new fromSection.AddSectionSuccess(data.section),
            new fromSection.SortSectionSuccess(data.ids)
          ]),
          catchError(error => of(new fromSection.AddSectionFail(error)))
        )
    )
  );

  @Effect()
  delete$: Observable<Action> = this.actions$.pipe(
    ofType(fromSection.DELETE_SECTION),
    mergeMap((action: fromSection.DeleteSection) =>
      this.sectionService
        .delete(action.id)
        .pipe(
          map(data => new fromSection.DeleteSectionSuccess(data)),
          catchError(error => of(new fromSection.DeleteSectionFail(error)))
        )
    )
  );

  @Effect()
  sort$: Observable<Action> = this.actions$.pipe(
    ofType(fromSection.SORT_SECTION),
    mergeMap((action: fromSection.SortSection) =>
      this.sectionService
        .sort(action.templateId, action.sectionIds)
        .pipe(
          map(data => new fromSection.SortSectionSuccess(data)),
          catchError(error => of(new fromSection.SortSectionFail(error)))
        )
    )
  );

  constructor(
    private actions$: Actions,
    private sectionService: SectionService
  ) {}
}
