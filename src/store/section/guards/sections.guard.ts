import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot } from "@angular/router";

import { Store } from "@ngrx/store";
import { Observable, of } from "rxjs";
import { tap, filter, take, switchMap, catchError, map } from "rxjs/operators";

import * as fromStore from "@store/section";

@Injectable()
export class SectionsGuard implements CanActivate {
  constructor(private store: Store<fromStore.SectionState>) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const templateId = parseInt(route.params.templateId, 10);
    return this.checkStore(templateId).pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  checkStore(templateId): Observable<boolean> {
    return this.store.select(fromStore.getSectionsByTemplate).pipe(
      tap(sections => {
        if (!sections.length) {
          this.store.dispatch(new fromStore.LoadSections(templateId));
        }
      }),
      map(sections => !!sections.length),
      take(1)
    );
  }
}
