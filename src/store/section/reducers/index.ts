import { EntityState, createEntityAdapter } from "@ngrx/entity";

import * as fromSections from "../actions";
import { TSection } from "../models";

export const adapter = createEntityAdapter<TSection>();

export interface SectionState extends EntityState<TSection> {
  loaded?: boolean;
  loading?: boolean;
  error?: any;
  selected: any;
  copyOfSelected: any;
}

export const initialState: SectionState = adapter.getInitialState({
  loaded: false,
  loading: false,
  selected: null,
  copyOfSelected: null
});

export function reducer(
  state: SectionState = initialState,
  action: fromSections.SectionsAction
): SectionState {
  switch (action.type) {
    case fromSections.LOAD_SECTIONS: {
      return {
        ...state,
        loaded: false,
        loading: true
      };
    }

    case fromSections.LOAD_SECTIONS_SUCCESS: {
      return {
        ...adapter.addMany(action.payload, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromSections.LOAD_SECTIONS_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }

    case fromSections.ADD_SECTION_SUCCESS: {
      return {
        ...adapter.addOne(action.payload, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromSections.ADD_SECTION_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }

    case fromSections.DELETE_SECTION_SUCCESS: {
      return {
        ...adapter.removeOne(action.payload.id, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromSections.SORT_SECTION_SUCCESS: {
      const temp = [];

      action.sectionIds.map((id, index) => {
        temp.push({
          id: id,
          changes: {
            position: index
          }
        });
      });

      return adapter.updateMany(temp, {
        ...state,
        loaded: true,
        loading: false
      });
    }

    case fromSections.SET_SELECTED_SECTION: {
      return {
        ...adapter.updateOne(
          {
            id: action.payload.id,
            changes: {
              isSelected: true
            }
          },
          state
        ),
        selected: action.payload,
        copyOfSelected: action.payload
      };
    }

    case fromSections.CLEAR_SELECTED_SECTION: {
      if (state.selected) {
        return {
          ...adapter.updateOne(
            {
              id: state.selected.id,
              changes: {
                isSelected: false
              }
            },
            state
          ),
          selected: null,
          copyOfSelected: null
        };
      } else {
        return {
          ...state,
          selected: null,
          copyOfSelected: null
        };
      }
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();
