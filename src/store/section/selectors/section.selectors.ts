import { createSelector, createFeatureSelector } from "@ngrx/store";

import * as fromRoot from "@store/router";
import * as fromReducer from "../reducers";

export const getSectionState = createFeatureSelector<fromReducer.SectionState>(
  "sections"
);

export const getAllSections = createSelector(
  getSectionState,
  fromReducer.selectAll
);

export const getSectionEntities = createSelector(
  getSectionState,
  fromReducer.selectEntities
);

export const getSelected = createSelector(
  getSectionState,
  (state: fromReducer.SectionState) => state.selected
);

export const getSectionsByTemplate = createSelector(
  getAllSections,
  fromRoot.getRouterState,
  (sections, router) => {
    const filtered = sections.filter(section => {
      return +router.state.params.templateId === section.templateId;
    });

    const sorted = filtered.sort((a, b) => {
      return a.position - b.position;
    });

    return sorted;
  }
);

export const getSectionsLoading = createSelector(
  getSectionState,
  (state: fromReducer.SectionState) => state.loading
);

export const getSectionsLoaded = createSelector(
  getSectionState,
  (state: fromReducer.SectionState) => state.loaded
);
