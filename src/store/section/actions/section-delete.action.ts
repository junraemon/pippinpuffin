import { Action } from "@ngrx/store";
import { TSection } from "../models";

// delete section
export const DELETE_SECTION = "[Section] Delete";
export const DELETE_SECTION_SUCCESS = "[Section] Delete Success";
export const DELETE_SECTION_FAIL = "[Section] Delete Fail";

export class DeleteSection implements Action {
  readonly type = DELETE_SECTION;
  constructor(public id: number) {}
}

export class DeleteSectionSuccess implements Action {
  readonly type = DELETE_SECTION_SUCCESS;
  constructor(public payload: TSection) {}
}

export class DeleteSectionFail implements Action {
  readonly type = DELETE_SECTION_FAIL;
  constructor(public payload: any) {}
}

// action types
export type DeleteSectionAction =
  | DeleteSection
  | DeleteSectionSuccess
  | DeleteSectionFail;
