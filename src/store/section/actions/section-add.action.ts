import { Action } from "@ngrx/store";
import { TSection } from "../models";

// add section
export const ADD_SECTION = "[Section] Add";
export const ADD_SECTION_SUCCESS = "[Section] Add Success";
export const ADD_SECTION_FAIL = "[Section] Add Fail";

export class AddSection implements Action {
  readonly type = ADD_SECTION;
  constructor(
    public templateId,
    public newSection: { type: number; position: number }
  ) {}
}

export class AddSectionSuccess implements Action {
  readonly type = ADD_SECTION_SUCCESS;
  constructor(public payload: TSection) {}
}

export class AddSectionFail implements Action {
  readonly type = ADD_SECTION_FAIL;
  constructor(public payload: TSection) {}
}

// action types
export type AddSectionAction = AddSection | AddSectionSuccess | AddSectionFail;
