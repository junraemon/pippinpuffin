import { LoadSectionsAction } from "./section-load.action";
import { AddSectionAction } from "./section-add.action";
import { DeleteSectionAction } from "./section-delete.action";
import { SortSectionAction } from "./section-sort.action";
import { SetSelectedSectionAction } from "./section-set-selected.action";

export type SectionsAction =
  | LoadSectionsAction
  | AddSectionAction
  | DeleteSectionAction
  | SortSectionAction
  | SetSelectedSectionAction;

export * from "./section-load.action";
export * from "./section-add.action";
export * from "./section-delete.action";
export * from "./section-sort.action";
export * from "./section-set-selected.action";
