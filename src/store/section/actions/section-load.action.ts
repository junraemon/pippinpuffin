import { Action } from "@ngrx/store";
import { TSection } from "../models";

// load sections
export const LOAD_SECTIONS = "[Sections] Load";
export const LOAD_SECTIONS_SUCCESS = "[Sections] Load Success";
export const LOAD_SECTIONS_FAIL = "[Sections] Load Fail";

export class LoadSections implements Action {
  readonly type = LOAD_SECTIONS;
  constructor(public templateId: number) {}
}

export class LoadSectionsSuccess implements Action {
  readonly type = LOAD_SECTIONS_SUCCESS;
  constructor(public payload: TSection[]) {}
}

export class LoadSectionsFail implements Action {
  readonly type = LOAD_SECTIONS_FAIL;
  constructor(public payload: any) {}
}

// action types
export type LoadSectionsAction =
  | LoadSections
  | LoadSectionsSuccess
  | LoadSectionsFail;
