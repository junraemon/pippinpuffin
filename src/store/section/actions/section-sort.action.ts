import { Action } from "@ngrx/store";
import { TSection } from "../models";

// sort section
export const SORT_SECTION = "[Section] Sort";
export const SORT_SECTION_SUCCESS = "[Section] Sort Success";
export const SORT_SECTION_FAIL = "[Section] Sort Fail";

export class SortSection implements Action {
  readonly type = SORT_SECTION;
  constructor(public templateId: number, public sectionIds: number[]) {}
}

export class SortSectionSuccess implements Action {
  readonly type = SORT_SECTION_SUCCESS;
  constructor(public sectionIds: number[]) {}
}

export class SortSectionFail implements Action {
  readonly type = SORT_SECTION_FAIL;
  constructor(public payload: any) {}
}

// action types
export type SortSectionAction =
  | SortSection
  | SortSectionSuccess
  | SortSectionFail;
