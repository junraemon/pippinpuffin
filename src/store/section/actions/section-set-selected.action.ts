import { Action } from "@ngrx/store";
import { TSection } from "../models";

// set selected section
export const SET_SELECTED_SECTION = "[Section] Set Selected";
export const UPDATE_SELECTED_SECTION = "[Section] Update Selected";
export const CLEAR_SELECTED_SECTION = "[Section] Clear Selected";

export class SetSelectedSection implements Action {
  readonly type = SET_SELECTED_SECTION;
  constructor(public payload: TSection) {}
}

export class UpdateSelectedSection implements Action {
  readonly type = UPDATE_SELECTED_SECTION;
  constructor(public payload: TSection) {}
}

export class ClearSelectedSection implements Action {
  readonly type = CLEAR_SELECTED_SECTION;
  constructor() {}
}

// action types
export type SetSelectedSectionAction =
  | SetSelectedSection
  | UpdateSelectedSection
  | ClearSelectedSection;
