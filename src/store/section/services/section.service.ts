import { throwError as observableThrowError, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { TSection } from "../models";
import { catchError } from "rxjs/operators";

@Injectable()
export class SectionService {
  private readonly endpoint = "/api/section";

  constructor(private http: HttpClient) {}

  getByTemplateId(templateId: number): Observable<TSection[]> {
    return this.http
      .get<TSection[]>(this.endpoint + "/template/" + templateId)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  add(
    templateId: number,
    section: { type: number; position: number }
  ): Observable<{ section: TSection; ids: number[] }> {
    return this.http
      .post<{ section: TSection; ids: number[] }>(
        `${this.endpoint}/add/${templateId}`,
        section
      )
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  sort(templateId: number, sectionIds: number[]): Observable<number[]> {
    return this.http
      .post<number[]>(`${this.endpoint}/sort/${templateId}`, sectionIds)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  delete(id: number): Observable<TSection> {
    return this.http
      .delete<TSection>(this.endpoint + "/" + id)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }
}
