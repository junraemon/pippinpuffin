import { CommonModule } from "@angular/common";
import { NgModule, ModuleWithProviders } from "@angular/core";
import { StoreModule, MetaReducer } from "@ngrx/store";
import { HttpClientModule } from "@angular/common/http";
import { EffectsModule } from "@ngrx/effects";
import {
  StoreRouterConnectingModule,
  RouterStateSerializer
} from "@ngrx/router-store";

// for development mode only
import { storeFreeze } from "ngrx-store-freeze";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";

import { CustomSerializer } from "./router/reducers";
import { reducers } from "./stores";
import { effects } from "./effects";
import { guards } from "./guards";
import { services } from "./services";

import { environment } from "../environments/environment";

export const metaReducers: MetaReducer<any>[] = !environment.production
  ? [storeFreeze]
  : [];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot(effects),
    StoreRouterConnectingModule,
    !environment.production
      ? StoreDevtoolsModule.instrument({
          maxAge: 25
        })
      : []
  ]
})
export class PStoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PStoreModule,
      providers: [
        ...guards,
        ...services,
        { provide: RouterStateSerializer, useClass: CustomSerializer }
      ]
    };
  }
}
