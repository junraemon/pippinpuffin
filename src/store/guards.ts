import { themeGuards } from "./theme/guards";
import { templateGuards } from "./template/guards";
import { sectionGuards } from "./section/guards";

export const guards: any[] = [
  ...themeGuards,
  ...templateGuards,
  ...sectionGuards
];

export * from "./theme/guards";
export * from "./template/guards";
export * from "./section/guards";
