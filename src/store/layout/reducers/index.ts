import { EntityState, createEntityAdapter } from "@ngrx/entity";

import * as fromLayouts from "../actions";
import { TLayout } from "../models";

export const adapter = createEntityAdapter<TLayout>();

export interface LayoutState extends EntityState<TLayout> {
  loaded?: boolean;
  loading?: boolean;
  error?: any;
  selected: any;
  copyOfSelected: any;
}

export const initialState: LayoutState = adapter.getInitialState({
  loaded: false,
  loading: false,
  selected: null,
  copyOfSelected: null
});

export function reducer(
  state: LayoutState = initialState,
  action: fromLayouts.LayoutsAction
): LayoutState {
  switch (action.type) {
    case fromLayouts.LOAD_LAYOUTS: {
      return {
        ...state,
        loaded: false,
        loading: true
      };
    }

    case fromLayouts.LOAD_LAYOUTS_SUCCESS: {
      return {
        ...adapter.addMany(action.payload, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromLayouts.ADD_LAYOUT_SUCCESS: {
      return {
        ...adapter.addOne(action.payload, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromLayouts.UPDATE_LAYOUT_SUCCESS: {
      return {
        ...adapter.updateOne(
          {
            id: action.payload.id,
            changes: action.payload
          },
          state
        ),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromLayouts.SORT_LAYOUT_SUCCESS: {
      const temp = [];

      action.layoutIds.map((id, index) => {
        temp.push({
          id: id,
          changes: {
            position: index
          }
        });
      });

      return adapter.updateMany(temp, {
        ...state,
        loaded: true,
        loading: false
      });
    }

    case fromLayouts.DELETE_LAYOUT_SUCCESS: {
      return {
        ...adapter.removeOne(action.payload.id, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromLayouts.LOAD_LAYOUTS_FAIL:
    case fromLayouts.ADD_LAYOUT_FAIL:
    case fromLayouts.UPDATE_LAYOUT_FAIL:
    case fromLayouts.DELETE_LAYOUT_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }

    case fromLayouts.SET_SELECTED_LAYOUT: {
      return {
        ...adapter.updateOne(
          {
            id: action.payload.id,
            changes: {
              isSelected: true
            }
          },
          state
        ),
        selected: action.payload,
        copyOfSelected: action.payload
      };
    }

    case fromLayouts.CLEAR_SELECTED_LAYOUT: {
      if (state.selected) {
        return {
          ...adapter.updateOne(
            {
              id: state.selected.id,
              changes: {
                isSelected: false
              }
            },
            state
          ),
          selected: null,
          copyOfSelected: null
        };
      } else {
        return {
          ...state,
          selected: null,
          copyOfSelected: null
        };
      }
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();
