import { Action } from "@ngrx/store";
import { TLayout, TSortLayout } from "../models";

// sort layout
export const SORT_LAYOUT = "[Layout] Sort";
export const SORT_LAYOUT_SUCCESS = "[Layout] Sort Success";
export const SORT_LAYOUT_FAIL = "[Layout] Sort Fail";

export class SortLayout implements Action {
  readonly type = SORT_LAYOUT;
  constructor(public sortLayout: TSortLayout) {}
}

export class SortLayoutSuccess implements Action {
  readonly type = SORT_LAYOUT_SUCCESS;
  constructor(public layoutIds: number[]) {}
}

export class SortLayoutFail implements Action {
  readonly type = SORT_LAYOUT_FAIL;
  constructor(public payload: any) {}
}

// action types
export type SortLayoutAction = SortLayout | SortLayoutSuccess | SortLayoutFail;
