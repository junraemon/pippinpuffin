import { Action } from "@ngrx/store";
import { TLayout } from "../models";

// update layout
export const UPDATE_LAYOUT = "[Layout] Update";
export const UPDATE_LAYOUT_SUCCESS = "[Layout] Update Success";
export const UPDATE_LAYOUT_FAIL = "[Layout] Update Fail";

export class UpdateLayout implements Action {
  readonly type = UPDATE_LAYOUT;
  constructor(public layoutId: number, public layout: Partial<TLayout>) {}
}

export class UpdateLayoutSuccess implements Action {
  readonly type = UPDATE_LAYOUT_SUCCESS;
  constructor(public payload: Partial<TLayout>) {}
}

export class UpdateLayoutFail implements Action {
  readonly type = UPDATE_LAYOUT_FAIL;
  constructor(public payload: any) {}
}

// action types
export type UpdateLayoutAction =
  | UpdateLayout
  | UpdateLayoutSuccess
  | UpdateLayoutFail;
