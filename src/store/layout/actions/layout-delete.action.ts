import { Action } from "@ngrx/store";
import { TLayout } from "../models";

// load layouts
export const DELETE_LAYOUT = "[Layout] Delete";
export const DELETE_LAYOUT_SUCCESS = "[Layout] Delete Success";
export const DELETE_LAYOUT_FAIL = "[Layout] Delete Fail";

export class DeleteLayout implements Action {
  readonly type = DELETE_LAYOUT;
  constructor(public layoutId: number) {}
}

export class DeleteLayoutSuccess implements Action {
  readonly type = DELETE_LAYOUT_SUCCESS;
  constructor(public payload: TLayout) {}
}

export class DeleteLayoutFail implements Action {
  readonly type = DELETE_LAYOUT_FAIL;
  constructor(public payload: any) {}
}

// action types
export type DeleteLayoutAction =
  | DeleteLayout
  | DeleteLayoutSuccess
  | DeleteLayoutFail;
