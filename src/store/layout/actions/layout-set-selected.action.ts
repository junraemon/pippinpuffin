import { Action } from "@ngrx/store";
import { TLayout } from "../models";

// set selected layout
export const SET_SELECTED_LAYOUT = "[Layout] Set Selected";
export const UPDATE_SELECTED_LAYOUT = "[Layout] Update Selected";
export const CLEAR_SELECTED_LAYOUT = "[Layout] Clear Selected";

export class SetSelectedLayout implements Action {
  readonly type = SET_SELECTED_LAYOUT;
  constructor(public payload: TLayout) {}
}

export class UpdateSelectedLayout implements Action {
  readonly type = UPDATE_SELECTED_LAYOUT;
  constructor(public payload: TLayout) {}
}

export class ClearSelectedLayout implements Action {
  readonly type = CLEAR_SELECTED_LAYOUT;
  constructor() {}
}

// action types
export type SetSelectedLayoutAction =
  | SetSelectedLayout
  | UpdateSelectedLayout
  | ClearSelectedLayout;
