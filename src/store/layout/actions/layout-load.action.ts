import { Action } from "@ngrx/store";
import { TLayout } from "../models";

// load layouts
export const LOAD_LAYOUTS = "[Layouts] Load";
export const LOAD_LAYOUTS_SUCCESS = "[Layouts] Load Success";
export const LOAD_LAYOUTS_FAIL = "[Layouts] Load Fail";

export class LoadLayouts implements Action {
  readonly type = LOAD_LAYOUTS;
  constructor(public sectionId: number) {}
}

export class LoadLayoutsSuccess implements Action {
  readonly type = LOAD_LAYOUTS_SUCCESS;
  constructor(public payload: TLayout[]) {}
}

export class LoadLayoutsFail implements Action {
  readonly type = LOAD_LAYOUTS_FAIL;
  constructor(public payload: any) {}
}

// action types
export type LoadLayoutsAction =
  | LoadLayouts
  | LoadLayoutsSuccess
  | LoadLayoutsFail;
