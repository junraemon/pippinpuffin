import { LoadLayoutsAction } from "./layout-load.action";
import { AddLayoutAction } from "./layout-add.action";
import { UpdateLayoutAction } from "./layout-update.action";
import { SortLayoutAction } from "./layout-sort.action";
import { DeleteLayoutAction } from "./layout-delete.action";
import { SetSelectedLayoutAction } from "./layout-set-selected.action";

export type LayoutsAction =
  | LoadLayoutsAction
  | AddLayoutAction
  | UpdateLayoutAction
  | SortLayoutAction
  | DeleteLayoutAction
  | SetSelectedLayoutAction;

export * from "./layout-load.action";
export * from "./layout-add.action";
export * from "./layout-update.action";
export * from "./layout-sort.action";
export * from "./layout-delete.action";
export * from "./layout-set-selected.action";
