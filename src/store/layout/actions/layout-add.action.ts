import { Action } from "@ngrx/store";
import { TLayout, TAddLayout } from "../models";

// add layouts
export const ADD_LAYOUT = "[Layout] Add";
export const ADD_LAYOUT_SUCCESS = "[Layout] Add Success";
export const ADD_LAYOUT_FAIL = "[Layout] Add Fail";

export class AddLayout implements Action {
  readonly type = ADD_LAYOUT;
  constructor(public addLayout: TAddLayout) {}
}

export class AddLayoutSuccess implements Action {
  readonly type = ADD_LAYOUT_SUCCESS;
  constructor(public payload: TLayout) {}
}

export class AddLayoutFail implements Action {
  readonly type = ADD_LAYOUT_FAIL;
  constructor(public payload: any) {}
}

// action types
export type AddLayoutAction = AddLayout | AddLayoutSuccess | AddLayoutFail;
