import { createSelector, createFeatureSelector } from "@ngrx/store";

import * as fromRoot from "@store/router";
import * as fromReducer from "../reducers";
import { TLayout } from "../models";

export const getLayoutState = createFeatureSelector<fromReducer.LayoutState>(
  "layouts"
);

export const getAllLayouts = createSelector(
  getLayoutState,
  fromReducer.selectAll
);

export const getLayoutEntities = createSelector(
  getLayoutState,
  fromReducer.selectEntities
);

export const getLayoutsLoading = createSelector(
  getLayoutState,
  (state: fromReducer.LayoutState) => state.loading
);

export const getLayoutsLoaded = createSelector(
  getLayoutState,
  (state: fromReducer.LayoutState) => state.loaded
);

export const getSelected = createSelector(
  getLayoutState,
  (state: fromReducer.LayoutState) => state.selected
);

export const getLayoutsBySectionId = (sectionId: number) =>
  createSelector(getAllLayouts, (layouts: TLayout[]) => {
    const filtered = layouts.filter(layout => {
      return sectionId === layout.componentSectionId;
    });

    const sorted = filtered.sort((a, b) => {
      return a.position - b.position;
    });

    return sorted;
  });
