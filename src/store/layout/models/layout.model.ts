export interface TLayout {
  position: number;
  componentSectionId: number;
  componentColumns: TColumn[];
  id?: number;
  lastUpdate?: Date;
  isSelected?: boolean;
}

export interface TColumn {
  componentLayoutId: number;
  position: number;
  layoutId: number;
  columnSizes: number;
  id?: number;
}

export interface TSortLayout {
  sectionId: number;
  layoutIds: number[];
  movedLayoutId?: number;
}

export interface TAddLayout {
  sectionId: number;
  numberOfColumns?: number;
  layoutId?: number;
  position: number;
}

export interface TAddLayoutSuccess {
  layout: TLayout;
  ids: number[];
}
