import { throwError as observableThrowError, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { TLayout, TSortLayout, TAddLayoutSuccess, TAddLayout } from "../models";
import { catchError } from "rxjs/operators";

@Injectable()
export class LayoutService {
  private readonly endpoint = "/api/layout";

  constructor(private http: HttpClient) {}

  getBySectionId(sectionId: number): Observable<TLayout[]> {
    return this.http
      .get<TLayout[]>(this.endpoint + "/section/" + sectionId)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  add(addLayout: TAddLayout): Observable<TAddLayoutSuccess> {
    return this.http
      .post<TAddLayoutSuccess>(this.endpoint, addLayout)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  sort(sortLayout: TSortLayout): Observable<TSortLayout> {
    return this.http
      .post<TSortLayout>(`${this.endpoint}/sort`, sortLayout)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  delete(id: number): Observable<TLayout> {
    return this.http
      .delete<TLayout>(this.endpoint + "/" + id)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }
}
