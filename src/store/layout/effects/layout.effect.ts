import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable ,  of } from "rxjs";

import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap, tap, switchMap } from "rxjs/operators";

import * as fromLayout from "../actions";
import { LayoutService } from "../services";
import { TLayout } from "../models";

@Injectable()
export class LayoutEffects {
  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType(fromLayout.LOAD_LAYOUTS),
    mergeMap((action: fromLayout.LoadLayouts) =>
      this.layoutService
        .getBySectionId(action.sectionId)
        .pipe(
          map(data => new fromLayout.LoadLayoutsSuccess(data)),
          catchError(error => of(new fromLayout.LoadLayoutsFail(error)))
        )
    )
  );

  @Effect()
  add$: Observable<Action> = this.actions$.pipe(
    ofType(fromLayout.ADD_LAYOUT),
    mergeMap((action: fromLayout.AddLayout) =>
      this.layoutService
        .add(action.addLayout)
        .pipe(
          mergeMap(data => [
            new fromLayout.AddLayoutSuccess(data.layout),
            new fromLayout.SortLayoutSuccess(data.ids)
          ]),
          catchError(error => of(new fromLayout.AddLayoutFail(error)))
        )
    )
  );

  @Effect()
  sort$: Observable<Action> = this.actions$.pipe(
    ofType(fromLayout.SORT_LAYOUT),
    mergeMap((action: fromLayout.SortLayout) =>
      this.layoutService.sort(action.sortLayout).pipe(
        mergeMap(data => [
          new fromLayout.SortLayoutSuccess(data.layoutIds),
          new fromLayout.UpdateLayoutSuccess({
            id: data.movedLayoutId,
            componentSectionId: action.sortLayout.sectionId
          })
        ]),
        catchError(error => of(new fromLayout.SortLayoutFail(error)))
      )
    )
  );

  @Effect()
  delete$: Observable<Action> = this.actions$.pipe(
    ofType(fromLayout.DELETE_LAYOUT),
    mergeMap((action: fromLayout.DeleteLayout) =>
      this.layoutService
        .delete(action.layoutId)
        .pipe(
          map(data => new fromLayout.DeleteLayoutSuccess(data)),
          catchError(error => of(new fromLayout.DeleteLayoutFail(error)))
        )
    )
  );

  constructor(
    private actions$: Actions,
    private layoutService: LayoutService
  ) {}
}
