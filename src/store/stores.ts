import { ActionReducerMap } from "@ngrx/store";
import * as fromRouter from "@ngrx/router-store";

import { RouterStateUrl } from "./router";
import * as fromTheme from "./theme/reducers";
import * as fromTemplate from "./template/reducers";
import * as fromSection from "./section/reducers";
import * as fromLayout from "./layout/reducers";
import * as fromContent from "./content/reducers";

export interface State {
  routerReducer: fromRouter.RouterReducerState<RouterStateUrl>;
  themes: fromTheme.ThemeState;
  templates: fromTemplate.TemplateState;
  sections: fromSection.SectionState;
  layouts: fromLayout.LayoutState;
  contents: fromContent.ContentState;
}

export const reducers: ActionReducerMap<State> = {
  routerReducer: fromRouter.routerReducer,
  themes: fromTheme.reducer,
  templates: fromTemplate.reducer,
  sections: fromSection.reducer,
  layouts: fromLayout.reducer,
  contents: fromContent.reducer
};
