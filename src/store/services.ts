import { themeServices } from "./theme/services";
import { templateServices } from "./template/services";
import { sectionServices } from "./section/services";
import { layoutServices } from "./layout/services";
import { contentServices } from "./content/services";

export const services: any[] = [
  ...themeServices,
  ...templateServices,
  ...sectionServices,
  ...layoutServices,
  ...contentServices
];

export * from "./theme/services";
export * from "./template/services";
export * from "./section/services";
export * from "./layout/services";
