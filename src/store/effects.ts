import { routerEffects } from "./router/effects";
import { themeEffects } from "./theme/effects";
import { templateEffects } from "./template/effects";
import { sectionEffects } from "./section/effects";
import { layoutEffects } from "./layout/effects";
import { contentEffects } from "./content/effects";

export const effects: any[] = [
  ...routerEffects,
  ...themeEffects,
  ...templateEffects,
  ...sectionEffects,
  ...layoutEffects,
  ...contentEffects
];

export * from "./router/effects";
export * from "./theme/effects";
export * from "./template/effects";
export * from "./section/effects";
export * from "./layout/effects";
export * from "./content/effects";
