export interface TContent {
  position: number;
  componentColumnId: number;
  id?: number;
  lastUpdate?: Date;
  data?: any;
  contentType?: string;
  contentTypeId?: number;
  backgroundColor: string;
  spacing: number;
  customClass: string;
  calloutStyle: number;
  isSelected?: boolean;
}

export interface TSortContent {
  columnId: number;
  contentIds: number[];
  movedContentId?: number;
}

export interface TAddContent {
  columnId: number;
  type: ContentType;
  typeId: number;
  position: number;
}

export interface TUpdateContent {
  backgroundColor: string;
  spacing: number;
  customClass: string;
  calloutStyle: number;
}

export interface TLoadContentSuccess {
  content: TContent;
  data: any;
}

export interface TAddContentSuccess {
  content: TContent;
  ids: number[];
  data: ContentText | ContentButton | ContentImage | null;
}

export enum ContentType {
  Text = "text",
  Button = "button",
  Image = "image"
}

export interface ContentText {
  id?: number;
  content: string;
  lastUpdate?: Date;
}

export interface ContentButton {
  id?: number;
  link: string;
  caption: string;
  alignment: string;
  size: string;
  type: string;
  corners: string;
  expanded: string;
  lastUpdate?: Date;
}

export interface ContentImage {
  id?: number;
  url: string;
  alignment: string;
  width: number;
  height: number;
  lastUpdate?: Date;
}
