import { Action } from "@ngrx/store";
import { TContent, TAddContent } from "../models";

// add content
export const ADD_CONTENT = "[Content] Add";
export const ADD_CONTENT_SUCCESS = "[Content] Add Success";
export const ADD_CONTENT_FAIL = "[Content] Add Fail";

export class AddContent implements Action {
  readonly type = ADD_CONTENT;
  constructor(public addContent: TAddContent) {}
}

export class AddContentSuccess implements Action {
  readonly type = ADD_CONTENT_SUCCESS;
  constructor(public payload: TContent) {}
}

export class AddContentFail implements Action {
  readonly type = ADD_CONTENT_FAIL;
  constructor(public payload: any) {}
}

// action types
export type AddContentAction = AddContent | AddContentSuccess | AddContentFail;
