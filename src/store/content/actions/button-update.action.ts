import { Action } from "@ngrx/store";
import { ContentButton } from "../models";

export const UPDATE_BUTTON = "[Button] Update";
export const UPDATE_BUTTON_SUCCESS = "[Button] Update Success";
export const UPDATE_BUTTON_FAIL = "[Button] Update Fail";

export class UpdateButton implements Action {
  readonly type = UPDATE_BUTTON;
  constructor(
    public payload: { buttonId: number; button: Partial<ContentButton> }
  ) {}
}

export class UpdateButtonFail implements Action {
  readonly type = UPDATE_BUTTON_FAIL;
  constructor(public payload: any) {}
}

export type UpdateButtonAction = UpdateButton | UpdateButtonFail;
