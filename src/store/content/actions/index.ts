import { LoadContentsAction } from "./content-load.action";
import { AddContentAction } from "./content-add.action";
import { UpdateContentAction } from "./content-update.action";
import { SaveContentAction } from "./content-save.action";
import { SortContentAction } from "./content-sort.action";
import { DeleteContentAction } from "./content-delete.action";
import { SetSelectedContentAction } from "./content-set-selected.action";
import { UpdateTextAction } from "./text-update.action";
import { UpdateButtonAction } from "./button-update.action";
import { UpdateImageAction } from "./image-update.action";

export type ContentsAction =
  | LoadContentsAction
  | AddContentAction
  | UpdateContentAction
  | SaveContentAction
  | SortContentAction
  | DeleteContentAction
  | SetSelectedContentAction
  | UpdateTextAction
  | UpdateButtonAction
  | UpdateImageAction;

export * from "./content-load.action";
export * from "./content-add.action";
export * from "./content-update.action";
export * from "./content-save.action";
export * from "./content-sort.action";
export * from "./content-delete.action";
export * from "./content-set-selected.action";
export * from "./text-update.action";
export * from "./button-update.action";
export * from "./image-update.action";
