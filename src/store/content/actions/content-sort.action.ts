import { Action } from "@ngrx/store";
import { TContent, TSortContent } from "../models";

// sort content
export const SORT_CONTENT = "[Content] Sort";
export const SORT_CONTENT_SUCCESS = "[Content] Sort Success";
export const SORT_CONTENT_FAIL = "[Content] Sort Fail";

export class SortContent implements Action {
  readonly type = SORT_CONTENT;
  constructor(public sortContent: TSortContent) {}
}

export class SortContentSuccess implements Action {
  readonly type = SORT_CONTENT_SUCCESS;
  constructor(public contentIds: number[]) {}
}

export class SortContentFail implements Action {
  readonly type = SORT_CONTENT_FAIL;
  constructor(public payload: any) {}
}

// action types
export type SortContentAction =
  | SortContent
  | SortContentSuccess
  | SortContentFail;
