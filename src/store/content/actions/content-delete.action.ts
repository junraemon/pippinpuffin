import { Action } from "@ngrx/store";
import { TContent } from "../models";

// load content
export const DELETE_CONTENT = "[Content] Delete";
export const DELETE_CONTENT_SUCCESS = "[Content] Delete Success";
export const DELETE_CONTENT_FAIL = "[Content] Delete Fail";

export class DeleteContent implements Action {
  readonly type = DELETE_CONTENT;
  constructor(public contentId: number) {}
}

export class DeleteContentSuccess implements Action {
  readonly type = DELETE_CONTENT_SUCCESS;
  constructor(public payload: TContent) {}
}

export class DeleteContentFail implements Action {
  readonly type = DELETE_CONTENT_FAIL;
  constructor(public payload: any) {}
}

// action types
export type DeleteContentAction =
  | DeleteContent
  | DeleteContentSuccess
  | DeleteContentFail;
