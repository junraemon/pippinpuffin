import { Action } from "@ngrx/store";
import { TContent, TUpdateContent } from "../models";

export const SAVE_CONTENT = "[Content] Save";
export const SAVE_CONTENT_SUCCESS = "[Content] Save Success";
export const SAVE_CONTENT_FAIL = "[Content] Save Fail";

export class SaveContent implements Action {
  readonly type = SAVE_CONTENT;
  constructor(public payload: { id: number; content: TUpdateContent }) {}
}

export class SaveContentSuccess implements Action {
  readonly type = SAVE_CONTENT_SUCCESS;
  constructor(public payload: Partial<TContent>) {}
}

export class SaveContentFail implements Action {
  readonly type = SAVE_CONTENT_FAIL;
  constructor(public payload: any) {}
}

export type SaveContentAction =
  | SaveContent
  | SaveContentSuccess
  | SaveContentFail;
