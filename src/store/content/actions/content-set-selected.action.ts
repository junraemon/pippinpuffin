import { Action } from "@ngrx/store";
import { TContent } from "../models";

// set selected content
export const SET_SELECTED_CONTENT = "[Content] Set Selected";
export const UPDATE_SELECTED_CONTENT = "[Content] Update Selected";
export const CLEAR_SELECTED_CONTENT = "[Content] Clear Selected";

export class SetSelectedContent implements Action {
  readonly type = SET_SELECTED_CONTENT;
  constructor(public payload: TContent) {}
}

export class UpdateSelectedContent implements Action {
  readonly type = UPDATE_SELECTED_CONTENT;
  constructor(public payload: TContent) {}
}

export class ClearSelectedContent implements Action {
  readonly type = CLEAR_SELECTED_CONTENT;
  constructor() {}
}

// action types
export type SetSelectedContentAction =
  | SetSelectedContent
  | UpdateSelectedContent
  | ClearSelectedContent;
