import { Action } from "@ngrx/store";
import { ContentImage } from "../models";

export const UPDATE_IMAGE = "[Image] Update";
export const UPDATE_IMAGE_SUCCESS = "[Image] Update Success";
export const UPDATE_IMAGE_FAIL = "[Image] Update Fail";

export class UpdateImage implements Action {
  readonly type = UPDATE_IMAGE;
  constructor(
    public payload: { imageId: number; image: Partial<ContentImage> }
  ) {}
}

export class UpdateImageFail implements Action {
  readonly type = UPDATE_IMAGE_FAIL;
  constructor(public payload: any) {}
}

export type UpdateImageAction = UpdateImage | UpdateImageFail;
