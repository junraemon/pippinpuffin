import { Action } from "@ngrx/store";
import { ContentText } from "../models";

// update text
export const UPDATE_TEXT = "[Text] Update";
export const UPDATE_TEXT_SUCCESS = "[Text] Update Success";
export const UPDATE_TEXT_FAIL = "[Text] Update Fail";

export class UpdateText implements Action {
  readonly type = UPDATE_TEXT;
  constructor(public payload: { textId: number; text: Partial<ContentText> }) {}
}

export class UpdateTextFail implements Action {
  readonly type = UPDATE_TEXT_FAIL;
  constructor(public payload: any) {}
}

// action types
export type UpdateTextAction = UpdateText | UpdateTextFail;
