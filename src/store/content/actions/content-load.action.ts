import { Action } from "@ngrx/store";
import { TContent } from "../models";

// load contents
export const LOAD_CONTENTS = "[Contents] Load";
export const LOAD_CONTENTS_SUCCESS = "[Contents] Load Success";
export const LOAD_CONTENTS_FAIL = "[Contents] Load Fail";

export class LoadContents implements Action {
  readonly type = LOAD_CONTENTS;
  constructor(public columnId: number) {}
}

export class LoadContentsSuccess implements Action {
  readonly type = LOAD_CONTENTS_SUCCESS;
  constructor(public payload: TContent[]) {}
}

export class LoadContentsFail implements Action {
  readonly type = LOAD_CONTENTS_FAIL;
  constructor(public payload: any) {}
}

// action types
export type LoadContentsAction =
  | LoadContents
  | LoadContentsSuccess
  | LoadContentsFail;
