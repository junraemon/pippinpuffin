import { Action } from "@ngrx/store";
import { TContent } from "../models";

// update content
export const UPDATE_CONTENT = "[Content] Update";
export const UPDATE_CONTENT_SUCCESS = "[Content] Update Success";
export const UPDATE_CONTENT_FAIL = "[Content] Update Fail";

export class UpdateContent implements Action {
  readonly type = UPDATE_CONTENT;
  constructor(public contentId: number, public content: Partial<TContent>) {}
}

export class UpdateContentSuccess implements Action {
  readonly type = UPDATE_CONTENT_SUCCESS;
  constructor(public payload: Partial<TContent>) {}
}

export class UpdateContentFail implements Action {
  readonly type = UPDATE_CONTENT_FAIL;
  constructor(public payload: any) {}
}

// action types
export type UpdateContentAction =
  | UpdateContent
  | UpdateContentSuccess
  | UpdateContentFail;
