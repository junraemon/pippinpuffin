import { createSelector, createFeatureSelector } from "@ngrx/store";

import * as fromRoot from "@store/router";
import * as fromReducer from "../reducers";
import { TContent } from "../models";

export const getContentState = createFeatureSelector<fromReducer.ContentState>(
  "contents"
);

export const getAllContents = createSelector(
  getContentState,
  fromReducer.selectAll
);

export const getLayoutEntities = createSelector(
  getContentState,
  fromReducer.selectEntities
);

export const getContentsLoading = createSelector(
  getContentState,
  (state: fromReducer.ContentState) => state.loading
);

export const getContentsLoaded = createSelector(
  getContentState,
  (state: fromReducer.ContentState) => state.loaded
);

export const getSelected = createSelector(
  getContentState,
  (state: fromReducer.ContentState) => state.selected
);

export const getCopyOfSelected = createSelector(
  getContentState,
  (state: fromReducer.ContentState) => state.copyOfSelected
);

export const getContentsByColumnId = (columnId: number) =>
  createSelector(getAllContents, (contents: TContent[]) => {
    const filtered = contents.filter(content => {
      return columnId === content.componentColumnId;
    });

    const sorted = filtered.sort((a, b) => {
      return a.position - b.position;
    });

    return sorted;
  });
