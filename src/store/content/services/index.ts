import { ContentService } from "./content.service";
import { TextService } from "./text.service";
import { ButtonService } from "./button.service";
import { ImageService } from "./image.service";

export const contentServices: any[] = [
  ContentService,
  TextService,
  ButtonService,
  ImageService
];

export * from "./content.service";
export * from "./text.service";
export * from "./button.service";
export * from "./image.service";
