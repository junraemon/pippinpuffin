import { throwError as observableThrowError, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import {
  TContent,
  TSortContent,
  TAddContent,
  TAddContentSuccess,
  TLoadContentSuccess,
  TUpdateContent
} from "../models";
import { catchError } from "rxjs/operators";

@Injectable()
export class ContentService {
  private readonly endpoint = "/api/content";

  constructor(private http: HttpClient) {}

  getByColumnId(columnId: number): Observable<TLoadContentSuccess[]> {
    return this.http
      .get<TLoadContentSuccess[]>(this.endpoint + "/column/" + columnId)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  add(addContent: TAddContent): Observable<TAddContentSuccess> {
    return this.http
      .post<TAddContentSuccess>(this.endpoint, addContent)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  sort(sortContent: TSortContent): Observable<TSortContent> {
    return this.http
      .post<TSortContent>(`${this.endpoint}/sort/`, sortContent)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  update(
    id: number,
    updateContent: TUpdateContent
  ): Observable<TAddContentSuccess> {
    return this.http
      .put<TAddContentSuccess>(`${this.endpoint}/${id}`, updateContent)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  delete(id: number): Observable<TContent> {
    return this.http
      .delete<TContent>(this.endpoint + "/" + id)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }
}
