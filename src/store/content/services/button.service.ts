import { throwError as observableThrowError, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { ContentButton, TAddContentSuccess } from "../models";
import { catchError } from "rxjs/operators";

@Injectable()
export class ButtonService {
  private readonly endpoint = "/api/button";

  constructor(private http: HttpClient) {}

  update(
    id: number,
    button: Partial<ContentButton>
  ): Observable<TAddContentSuccess> {
    return this.http
      .put<TAddContentSuccess>(`${this.endpoint}/${id}`, button)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }
}
