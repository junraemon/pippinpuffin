import { throwError as observableThrowError, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { ContentText, TAddContentSuccess } from "../models";
import { catchError } from "rxjs/operators";

@Injectable()
export class TextService {
  private readonly endpoint = "/api/text";

  constructor(private http: HttpClient) {}

  update(id: number, text: Partial<ContentText>): Observable<TAddContentSuccess> {
    return this.http
      .put<TAddContentSuccess>(`${this.endpoint}/${id}`, text)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }
}
