import { throwError as observableThrowError, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { ContentImage, TAddContentSuccess } from "../models";
import { catchError } from "rxjs/operators";

@Injectable()
export class ImageService {
  private readonly endpoint = "/api/image";

  constructor(private http: HttpClient) {}

  update(
    id: number,
    image: Partial<ContentImage>
  ): Observable<TAddContentSuccess> {
    return this.http
      .put<TAddContentSuccess>(`${this.endpoint}/${id}`, image)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }
}
