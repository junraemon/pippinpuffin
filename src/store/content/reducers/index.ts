import { EntityState, createEntityAdapter } from "@ngrx/entity";

import * as fromContents from "../actions";
import { TContent } from "../models";

export const adapter = createEntityAdapter<TContent>();

export interface ContentState extends EntityState<TContent> {
  loaded: boolean;
  loading: boolean;
  error?: any;
  selected: any;
  copyOfSelected: any;
}

export const initialState: ContentState = adapter.getInitialState({
  loaded: false,
  loading: false,
  selected: null,
  copyOfSelected: null
});

export function reducer(
  state: ContentState = initialState,
  action: fromContents.ContentsAction
): ContentState {
  switch (action.type) {
    case fromContents.LOAD_CONTENTS: {
      return {
        ...state,
        loaded: false,
        loading: true
      };
    }

    case fromContents.LOAD_CONTENTS_SUCCESS: {
      return {
        ...adapter.addMany(action.payload, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromContents.ADD_CONTENT_SUCCESS: {
      return {
        ...adapter.addOne(action.payload, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromContents.UPDATE_CONTENT_SUCCESS: {
      return {
        ...adapter.updateOne(
          {
            id: action.payload.id,
            changes: action.payload
          },
          state
        ),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromContents.SORT_CONTENT_SUCCESS: {
      const temp = [];

      action.contentIds.map((id, index) => {
        temp.push({
          id: id,
          changes: {
            position: index
          }
        });
      });

      return adapter.updateMany(temp, {
        ...state,
        loaded: true,
        loading: false
      });
    }

    case fromContents.DELETE_CONTENT_SUCCESS: {
      return {
        ...adapter.removeOne(action.payload.id, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromContents.LOAD_CONTENTS_FAIL:
    case fromContents.ADD_CONTENT_FAIL:
    case fromContents.UPDATE_CONTENT_FAIL:
    case fromContents.SORT_CONTENT_FAIL:
    case fromContents.DELETE_CONTENT_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }

    case fromContents.SET_SELECTED_CONTENT: {
      return {
        ...adapter.updateOne(
          {
            id: action.payload.id,
            changes: {
              isSelected: true
            }
          },
          state
        ),
        selected: action.payload,
        copyOfSelected: action.payload
      };
    }

    case fromContents.UPDATE_SELECTED_CONTENT: {
      return {
        ...state,
        copyOfSelected: action.payload
      };
    }

    case fromContents.CLEAR_SELECTED_CONTENT: {
      if (state.selected) {
        return {
          ...adapter.updateOne(
            {
              id: state.selected.id,
              changes: {
                isSelected: false
              }
            },
            state
          ),
          selected: null,
          copyOfSelected: null
        };
      } else {
        return {
          ...state,
          selected: null,
          copyOfSelected: null
        };
      }
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();
