import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";

import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap } from "rxjs/operators";

import * as fromContent from "../actions";
import { ButtonService } from "../services";
import {
  TLoadContentSuccess,
  TAddContentSuccess
} from "./../models/content.model";

@Injectable()
export class ButtonEffects {
  @Effect()
  updateTemplate$: Observable<Action> = this.actions$.pipe(
    ofType(fromContent.UPDATE_BUTTON),
    mergeMap((action: fromContent.UpdateButton) =>
      this.buttonService
        .update(action.payload.buttonId, action.payload.button)
        .pipe(
          map(data => {
            return {
              ...data.content,
              data: data.data
            };
          }),
          map((data: any) => new fromContent.UpdateContentSuccess(data)),
          catchError(error => of(new fromContent.UpdateButtonFail(error)))
        )
    )
  );

  constructor(
    private actions$: Actions,
    private buttonService: ButtonService
  ) {}
}
