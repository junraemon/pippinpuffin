import { ContentEffects } from "./content.effect";
import { TextEffects } from "./text.effect";
import { ButtonEffects } from "./button.effect";
import { ImageEffects } from "./image.effect";

export const contentEffects = [
  ContentEffects,
  TextEffects,
  ButtonEffects,
  ImageEffects
];

export * from "./content.effect";
export * from "./text.effect";
export * from "./button.effect";
export * from "./image.effect";
