import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";

import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap } from "rxjs/operators";

import * as fromContent from "../actions";
import { ImageService } from "../services";
import { TLoadContentSuccess } from "./../models/content.model";

@Injectable()
export class ImageEffects {
  @Effect()
  updateTemplate$: Observable<Action> = this.actions$.pipe(
    ofType(fromContent.UPDATE_IMAGE),
    mergeMap((action: fromContent.UpdateImage) =>
      this.imageService
        .update(action.payload.imageId, action.payload.image)
        .pipe(
          map(data => {
            return {
              ...data.content,
              data: data.data
            };
          }),
          map((data: any) => new fromContent.UpdateContentSuccess(data)),
          catchError(error => of(new fromContent.UpdateImageFail(error)))
        )
    )
  );

  constructor(private actions$: Actions, private imageService: ImageService) {}
}
