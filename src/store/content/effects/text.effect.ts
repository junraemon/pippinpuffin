import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";

import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap } from "rxjs/operators";

import * as fromContent from "../actions";
import { TextService } from "../services";
import { TLoadContentSuccess } from "./../models/content.model";

@Injectable()
export class TextEffects {
  @Effect()
  updateTemplate$: Observable<Action> = this.actions$.pipe(
    ofType(fromContent.UPDATE_TEXT),
    mergeMap((action: fromContent.UpdateText) =>
      this.textService.update(action.payload.textId, action.payload.text).pipe(
        map(data => {
          return {
            ...data.content,
            data: data.data
          };
        }),
        map((data: any) => new fromContent.UpdateContentSuccess(data)),
        catchError(error => of(new fromContent.UpdateTextFail(error)))
      )
    )
  );

  constructor(private actions$: Actions, private textService: TextService) {}
}
