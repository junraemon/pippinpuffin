import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap } from "rxjs/operators";

import * as fromContent from "../actions";
import { ContentService } from "../services";
import { TContent } from "../models";

@Injectable()
export class ContentEffects {
  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType(fromContent.LOAD_CONTENTS),
    mergeMap((action: fromContent.LoadContents) =>
      this.contentService.getByColumnId(action.columnId).pipe(
        map(data => {
          return data.map(d => {
            return { ...d.content, data: d.data };
          });
        }),
        map(data => new fromContent.LoadContentsSuccess(data)),
        catchError(error => of(new fromContent.LoadContentsFail(error)))
      )
    )
  );

  @Effect()
  add$: Observable<Action> = this.actions$.pipe(
    ofType(fromContent.ADD_CONTENT),
    mergeMap((action: fromContent.AddContent) =>
      this.contentService.add(action.addContent).pipe(
        map(data => {
          return {
            ...data,
            content: { ...data.content, data: data.data }
          };
        }),
        mergeMap(data => [
          new fromContent.AddContentSuccess(data.content),
          new fromContent.SortContentSuccess(data.ids)
        ]),
        catchError(error => of(new fromContent.AddContentFail(error)))
      )
    )
  );

  @Effect()
  sort$: Observable<Action> = this.actions$.pipe(
    ofType(fromContent.SORT_CONTENT),
    mergeMap((action: fromContent.SortContent) =>
      this.contentService.sort(action.sortContent).pipe(
        mergeMap(data => [
          new fromContent.SortContentSuccess(data.contentIds),
          new fromContent.UpdateContentSuccess({
            id: data.movedContentId,
            componentColumnId: action.sortContent.columnId
          })
        ]),
        catchError(error => of(new fromContent.SortContentFail(error)))
      )
    )
  );

  @Effect()
  delete$: Observable<Action> = this.actions$.pipe(
    ofType(fromContent.DELETE_CONTENT),
    mergeMap((action: fromContent.DeleteContent) =>
      this.contentService.delete(action.contentId).pipe(
        map(data => new fromContent.DeleteContentSuccess(data)),
        catchError(error => of(new fromContent.DeleteContentFail(error)))
      )
    )
  );

  @Effect()
  update$: Observable<Action> = this.actions$.pipe(
    ofType(fromContent.SAVE_CONTENT),
    mergeMap((action: fromContent.SaveContent) =>
      this.contentService
        .update(action.payload.id, action.payload.content)
        .pipe(
          map(data => {
            return {
              ...data.content,
              data: data.data
            };
          }),
          mergeMap((data: TContent) => [
            new fromContent.UpdateContentSuccess(data),
            new fromContent.SetSelectedContent(data)
          ]),
          catchError(error => of(new fromContent.UpdateButtonFail(error)))
        )
    )
  );

  constructor(
    private actions$: Actions,
    private contentService: ContentService
  ) {}
}
