import { createSelector, createFeatureSelector } from "@ngrx/store";

import * as fromRoot from "@store/router";
import * as fromReducer from "../reducers";
import { Theme } from "../models";

export const getThemeState = createFeatureSelector<fromReducer.ThemeState>(
  "themes"
);

export const getAllThemes = createSelector(
  getThemeState,
  fromReducer.selectAll
);

export const getThemesEntities = createSelector(
  getThemeState,
  fromReducer.selectEntities
);

export const getSelectedTheme = createSelector(
  getThemesEntities,
  fromRoot.getRouterState,
  (entities, router): Theme => {
    return router && entities[router.state.params.themeId];
  }
);

export const getThemesLoaded = createSelector(
  getThemeState,
  (state: fromReducer.ThemeState) => state.loaded
);

export const getThemesLoading = createSelector(
  getThemeState,
  (state: fromReducer.ThemeState) => state.loading
);

export const getThemesError = createSelector(
  getThemeState,
  (state: fromReducer.ThemeState) => state.error
);
