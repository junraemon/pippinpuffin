import { EntityState, EntityAdapter, createEntityAdapter } from "@ngrx/entity";
import { createFeatureSelector } from "@ngrx/store";

import * as fromThemes from "../actions";
import { Theme } from "../models";

export const adapter = createEntityAdapter<Theme>();

export interface ThemeState extends EntityState<Theme> {
  loaded?: boolean;
  loading?: boolean;
  error?: any;
}

export const initialState: ThemeState = adapter.getInitialState({
  loaded: false,
  loading: false
});

export function reducer(
  state: ThemeState = initialState,
  action: fromThemes.ThemesAction
): ThemeState {
  switch (action.type) {
    case fromThemes.LOAD_THEMES: {
      return {
        ...state,
        loaded: false,
        loading: true
      };
    }

    case fromThemes.LOAD_THEMES_SUCCESS: {
      return {
        ...adapter.addAll(action.payload, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromThemes.LOAD_THEMES_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }

    case fromThemes.CREATE_THEME_SUCCESS: {
      return {
        ...adapter.addOne(action.payload, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromThemes.CREATE_THEME_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }

    case fromThemes.CUSTOMIZE_THEME_SUCCESS:
    case fromThemes.UPDATE_THEME_SUCCESS: {
      return {
        ...adapter.updateOne(
          {
            id: action.payload.id,
            changes: action.payload
          },
          state
        ),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromThemes.CUSTOMIZE_THEME_FAIL:
    case fromThemes.UPDATE_THEME_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }
    case fromThemes.DELETE_THEME_SUCCESS: {
      return {
        ...adapter.removeOne(action.payload, state),
        loaded: true,
        loading: false
      };
    }

    case fromThemes.DELETE_THEME_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();
