import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable ,  of } from "rxjs";

import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap, tap, switchMap } from "rxjs/operators";

import * as fromThemes from "../actions";
import { ThemeService } from "../services";
import { Theme } from "../models";

@Injectable()
export class ThemeEffects {
  @Effect()
  loadThemes$: Observable<Action> = this.actions$.pipe(
    ofType(fromThemes.LOAD_THEMES),
    mergeMap(action =>
      this.themeService
        .getThemes()
        .pipe(
          map(data => new fromThemes.LoadThemesSuccess(data)),
          catchError(error => of(new fromThemes.LoadThemesFail(error)))
        )
    )
  );

  @Effect()
  addTheme$: Observable<Action> = this.actions$.pipe(
    ofType(fromThemes.CREATE_THEME),
    mergeMap((action: fromThemes.CreateTheme) =>
      this.themeService
        .create(action.payload)
        .pipe(
          map((data: Theme) => new fromThemes.CreateThemeSuccess(data)),
          catchError(error => of(new fromThemes.LoadThemesFail(error)))
        )
    )
  );

  @Effect()
  updateTheme$: Observable<Action> = this.actions$.pipe(
    ofType(fromThemes.UPDATE_THEME),
    mergeMap((action: fromThemes.UpdateTheme) =>
      this.themeService
        .update(action.payload.id, action.payload)
        .pipe(
          map((data: Theme) => new fromThemes.UpdateThemeSuccess(data)),
          catchError(error => of(new fromThemes.UpdateThemeFail(error)))
        )
    )
  );

  @Effect()
  customizeTheme$: Observable<Action> = this.actions$.pipe(
    ofType(fromThemes.CUSTOMIZE_THEME),
    mergeMap((action: fromThemes.CustomizeTheme) =>
      this.themeService
        .update(action.payload.id, action.payload)
        .pipe(
          map((data: Theme) => new fromThemes.CustomizeThemeSuccess(data)),
          catchError(error => of(new fromThemes.CustomizeThemeFail(error)))
        )
    )
  );

  @Effect()
  deleteTheme$: Observable<Action> = this.actions$.pipe(
    ofType(fromThemes.DELETE_THEME),
    mergeMap((action: fromThemes.DeleteTheme) =>
      this.themeService
        .delete(action.payload)
        .pipe(
          map((data: number) => new fromThemes.DeleteThemeSuccess(data)),
          catchError(error => of(new fromThemes.DeleteThemeFail(error)))
        )
    )
  );

  @Effect({ dispatch: false })
  addThemeSuccess$: Observable<Action> = this.actions$.pipe(
    ofType(
      fromThemes.CREATE_THEME_SUCCESS,
      fromThemes.UPDATE_THEME_SUCCESS,
      fromThemes.DELETE_THEME_SUCCESS
    ),
    tap(() => this.router.navigate(["/themes"]))
  );

  constructor(
    private actions$: Actions,
    private themeService: ThemeService,
    private router: Router
  ) {}
}
