import { throwError as observableThrowError, Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { catchError } from "rxjs/operators";

import { Theme } from "../models";

@Injectable()
export class ThemeService {
  private readonly endpoint = "/api/theme";
  constructor(private http: HttpClient) {}

  getThemes(): Observable<Theme[]> {
    return this.http
      .get<Theme[]>(this.endpoint)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  getTheme(id): Observable<Theme> {
    return this.http
      .get<Theme>(this.endpoint + "/" + id)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  create(theme): Observable<Theme> {
    return this.http
      .post<Theme>(this.endpoint, theme)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  update(id, theme): Observable<Theme> {
    return this.http
      .put<Theme>(this.endpoint + "/" + id, theme)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }

  delete(id): Observable<number> {
    return this.http
      .delete<number>(this.endpoint + "/" + id)
      .pipe(catchError((error: any) => observableThrowError(error.json())));
  }
}
