import { ThemesGuard } from "./themes.guard";
import { ThemeExistsGuards } from "./theme-exists.guard";

export const themeGuards: any[] = [ThemesGuard, ThemeExistsGuards];

export * from "./themes.guard";
export * from "./theme-exists.guard";
