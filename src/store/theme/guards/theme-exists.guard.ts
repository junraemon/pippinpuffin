import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { tap, map, filter, take, switchMap } from "rxjs/operators";

import * as fromStore from "@store/theme";
import * as fromRouterStore from "@store/router";

@Injectable()
export class ThemeExistsGuards implements CanActivate {
  constructor(
    private store: Store<fromStore.ThemeState>,
    private routerStore: Store<fromRouterStore.RouterStateUrl>
  ) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => {
        const themeId = parseInt(route.params.themeId, 10);
        return this.hasTheme(themeId);
      })
    );
  }

  hasTheme(themeId: number): Observable<boolean> {
    return this.store.select(fromStore.getThemesEntities).pipe(
      map(
        (entities: { [key: number]: fromStore.Theme }) => !!entities[themeId]
      ),
      take(1),
      tap(entities => {
        if (!entities) {
          this.routerStore.dispatch(
            new fromRouterStore.Go({
              path: [`/`]
            })
          );
        }
      })
    );
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getThemesLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(new fromStore.LoadThemes());
        }
      }),
      filter(loaded => loaded),
      take(1)
    );
  }
}
