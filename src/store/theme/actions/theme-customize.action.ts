import { Action } from "@ngrx/store";

import { Theme } from "../models";

// customize theme
export const CUSTOMIZE_THEME = "[Theme] Customize Theme";
export const CUSTOMIZE_THEME_SUCCESS = "[Theme] Customize Theme Success";
export const CUSTOMIZE_THEME_FAIL = "[Theme] Customize Theme Fail";

export class CustomizeTheme implements Action {
  readonly type = CUSTOMIZE_THEME;
  constructor(public payload: Theme) {}
}

export class CustomizeThemeSuccess implements Action {
  readonly type = CUSTOMIZE_THEME_SUCCESS;
  constructor(public payload: Theme) {}
}

export class CustomizeThemeFail implements Action {
  readonly type = CUSTOMIZE_THEME_FAIL;
  constructor(public payload: Theme) {}
}

// action types
export type CustomizeThemeAction =
  | CustomizeTheme
  | CustomizeThemeSuccess
  | CustomizeThemeFail;
