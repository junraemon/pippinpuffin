import { Action } from "@ngrx/store";

import { Theme } from "../models";

// update theme
export const UPDATE_THEME = "[Theme] Update Theme";
export const UPDATE_THEME_SUCCESS = "[Theme] Update Theme Success";
export const UPDATE_THEME_FAIL = "[Theme] Update Theme Fail";

export class UpdateTheme implements Action {
  readonly type = UPDATE_THEME;
  constructor(public payload: Theme) {}
}

export class UpdateThemeSuccess implements Action {
  readonly type = UPDATE_THEME_SUCCESS;
  constructor(public payload: Theme) {}
}

export class UpdateThemeFail implements Action {
  readonly type = UPDATE_THEME_FAIL;
  constructor(public payload: Theme) {}
}

// action types
export type UpateThemeAction =
  | UpdateTheme
  | UpdateThemeSuccess
  | UpdateThemeFail;
