import { Action } from "@ngrx/store";

import { Theme } from "../models";

// delete theme
export const DELETE_THEME = "[Theme] Delete Theme";
export const DELETE_THEME_SUCCESS = "[Theme] Delete Theme Success";
export const DELETE_THEME_FAIL = "[Theme] Delete Theme Fail";

export class DeleteTheme implements Action {
  readonly type = DELETE_THEME;
  constructor(public payload: number) {}
}

export class DeleteThemeSuccess implements Action {
  readonly type = DELETE_THEME_SUCCESS;
  constructor(public payload: number) {}
}

export class DeleteThemeFail implements Action {
  readonly type = DELETE_THEME_FAIL;
  constructor(public payload: number) {}
}

// action types
export type DeleteThemeAction =
  | DeleteTheme
  | DeleteThemeSuccess
  | DeleteThemeFail;
