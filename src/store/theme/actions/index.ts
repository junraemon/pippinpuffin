import { LoadThemeAction } from "./theme-load.action";
import { CreateThemeAction } from "./theme-create.action";
import { UpateThemeAction } from "./theme-update.action";
import { CustomizeThemeAction } from "./theme-customize.action";
import { DeleteThemeAction } from "./theme-delete.action";

// action types
export type ThemesAction =
  | LoadThemeAction
  | CreateThemeAction
  | CustomizeThemeAction
  | UpateThemeAction
  | DeleteThemeAction;

export * from "./theme-load.action";
export * from "./theme-create.action";
export * from "./theme-update.action";
export * from "./theme-customize.action";
export * from "./theme-delete.action";
