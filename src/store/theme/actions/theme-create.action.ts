import { Action } from "@ngrx/store";

import { Theme } from "../models";

// create theme
export const CREATE_THEME = "[Theme] Create Theme";
export const CREATE_THEME_SUCCESS = "[Theme] Create Theme Success";
export const CREATE_THEME_FAIL = "[Theme] Create Theme Fail";

export class CreateTheme implements Action {
  readonly type = CREATE_THEME;
  constructor(public payload: Theme) {}
}

export class CreateThemeSuccess implements Action {
  readonly type = CREATE_THEME_SUCCESS;
  constructor(public payload: Theme) {}
}

export class CreateThemeFail implements Action {
  readonly type = CREATE_THEME_FAIL;
  constructor(public payload: Theme) {}
}

// action types
export type CreateThemeAction =
  | CreateTheme
  | CreateThemeSuccess
  | CreateThemeFail;
