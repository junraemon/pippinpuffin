import { Action } from "@ngrx/store";

import { Theme } from "../models";

// load themes
export const LOAD_THEMES = "[Theme] Load Themes";
export const LOAD_THEMES_SUCCESS = "[Theme] Load Themes Success";
export const LOAD_THEMES_FAIL = "[Theme] Load Themes Fail";

export class LoadThemes implements Action {
  readonly type = LOAD_THEMES;
}

export class LoadThemesSuccess implements Action {
  readonly type = LOAD_THEMES_SUCCESS;
  constructor(public payload: Theme[]) {}
}

export class LoadThemesFail implements Action {
  readonly type = LOAD_THEMES_FAIL;
  constructor(public payload: any) {}
}

// action types
export type LoadThemeAction = LoadThemes | LoadThemesSuccess | LoadThemesFail;
