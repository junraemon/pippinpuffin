export interface Theme {
  name: string;
  customCss: string;
  description: string;
  id?: number;
  lastUpdate?: Date;
}
