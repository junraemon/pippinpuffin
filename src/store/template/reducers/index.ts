import { EntityState, EntityAdapter, createEntityAdapter } from "@ngrx/entity";
import { createFeatureSelector } from "@ngrx/store";

import * as fromTemplates from "../actions";
import { Template } from "../models";

export const adapter = createEntityAdapter<Template>();

export interface TemplateState extends EntityState<Template> {
  loaded?: boolean;
  loading?: boolean;
  error?: any;
}

export const initialState: TemplateState = adapter.getInitialState({
  loaded: false,
  loading: false
});

export function reducer(
  state: TemplateState = initialState,
  action: fromTemplates.TemplatesAction
): TemplateState {
  switch (action.type) {
    case fromTemplates.LOAD_TEMPLATES: {
      return {
        ...state,
        loaded: false,
        loading: true
      };
    }

    case fromTemplates.LOAD_TEMPLATES_SUCCESS: {
      return {
        ...adapter.addMany(action.payload, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromTemplates.LOAD_TEMPLATES_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }

    case fromTemplates.CREATE_TEMPLATE_SUCCESS: {
      return {
        ...adapter.addOne(action.payload, state),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromTemplates.CREATE_TEMPLATE_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }

    case fromTemplates.UPDATE_TEMPLATE_SUCCESS: {
      return {
        ...adapter.updateOne(
          {
            id: action.payload.id,
            changes: action.payload
          },
          state
        ),
        loaded: true,
        loading: false,
        error: null
      };
    }

    case fromTemplates.UPDATE_TEMPLATE_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }

    case fromTemplates.DELETE_TEMPLATE_SUCCESS: {
      return {
        ...adapter.removeOne(action.payload.id, state),
        loaded: true,
        loading: false
      };
    }

    case fromTemplates.DELETE_TEMPLATE_FAIL: {
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();
