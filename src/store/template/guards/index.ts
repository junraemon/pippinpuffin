import { TemplatesGuard } from "./templates.guard";
import { TemplateExistsGuards } from "./templates.exists.guard";

export const templateGuards: any[] = [TemplatesGuard, TemplateExistsGuards];

export * from "./templates.guard";
export * from "./templates.exists.guard";
