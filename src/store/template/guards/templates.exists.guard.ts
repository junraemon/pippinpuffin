import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { tap, map, filter, take, switchMap } from "rxjs/operators";

import * as fromStore from "@store/template";

@Injectable()
export class TemplateExistsGuards implements CanActivate {
  constructor(private store: Store<fromStore.TemplateState>) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const themeId = parseInt(route.params.themeId, 10);
    return this.checkStore(themeId).pipe(
      switchMap(() => {
        const templateId = parseInt(route.params.templateId, 10);
        return this.hasTheme(templateId);
      })
    );
  }

  hasTheme(id: number): Observable<boolean> {
    return this.store
      .select(fromStore.getTemplateEntities)
      .pipe(
        map(
          (entities: { [key: number]: fromStore.Template }) => !!entities[id]
        ),
        take(1)
      );
  }

  checkStore(themeId): Observable<boolean> {
    return this.store.select(fromStore.getTemplatesLoaded).pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(new fromStore.LoadTemplates(themeId));
        }
      }),
      filter(loaded => loaded),
      take(1)
    );
  }
}
