import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot } from "@angular/router";

import { Store } from "@ngrx/store";
import { Observable, of } from "rxjs";
import { tap, filter, take, switchMap, catchError, map } from "rxjs/operators";

import * as fromStore from "@store/template";

@Injectable()
export class TemplatesGuard implements CanActivate {
  constructor(private store: Store<fromStore.TemplateState>) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const themeId = parseInt(route.params.themeId, 10);
    return this.checkStore(themeId).pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  checkStore(themeId): Observable<boolean> {
    return this.store.select(fromStore.getTemplatesByTheme).pipe(
      tap(templates => {
        if (!templates.length) {
          this.store.dispatch(new fromStore.LoadTemplates(themeId));
        }
      }),
      map(templates => !!templates.length),
      take(1)
    );
  }
}
