import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable ,  of } from "rxjs";

import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap, tap, switchMap } from "rxjs/operators";

import * as fromTemplates from "../actions";
import { TemplateService } from "../services";
import { Template } from "../models";

@Injectable()
export class TemplateEffects {
  @Effect()
  loadTemplate$: Observable<Action> = this.actions$.pipe(
    ofType(fromTemplates.LOAD_TEMPLATES),
    mergeMap((action: fromTemplates.LoadTemplates) =>
      this.templateServe
        .getByThemeId(action.themeId)
        .pipe(
          map(data => new fromTemplates.LoadTemplatesSuccess(data)),
          catchError(error => of(new fromTemplates.LoadTemplates(error)))
        )
    )
  );

  @Effect()
  addTemplate$: Observable<Action> = this.actions$.pipe(
    ofType(fromTemplates.CREATE_TEMPLATE),
    mergeMap((action: fromTemplates.CreateTemplate) =>
      this.templateServe
        .create(action.payload)
        .pipe(
          map(
            (data: Template) => new fromTemplates.CreateTemplateSuccess(data)
          ),
          catchError(error => of(new fromTemplates.CreateTemplateFail(error)))
        )
    )
  );

  @Effect()
  updateTemplate$: Observable<Action> = this.actions$.pipe(
    ofType(fromTemplates.UPDATE_TEMPLATE),
    mergeMap((action: fromTemplates.UpdateTemplate) =>
      this.templateServe
        .update(action.payload)
        .pipe(
          map(
            (data: Template) => new fromTemplates.UpdateTemplateSuccess(data)
          ),
          catchError(error => of(new fromTemplates.UpdateTemplateFail(error)))
        )
    )
  );

  @Effect()
  deleteTemplate$: Observable<Action> = this.actions$.pipe(
    ofType(fromTemplates.DELETE_TEMPLATE),
    mergeMap((action: fromTemplates.DeleteTemplate) =>
      this.templateServe
        .delete(action.payload)
        .pipe(
          map(
            (data: Template) => new fromTemplates.DeleteTemplateSuccess(data)
          ),
          catchError(error => of(new fromTemplates.DeleteTemplateFail(error)))
        )
    )
  );

  @Effect({ dispatch: false })
  navigateOnSuccess$: Observable<Action> = this.actions$.pipe(
    ofType(
      fromTemplates.CREATE_TEMPLATE_SUCCESS,
      fromTemplates.UPDATE_TEMPLATE_SUCCESS,
      fromTemplates.DELETE_TEMPLATE_SUCCESS
    ),
    tap((data: fromTemplates.CreateTemplateSuccess) => {
      const themeId = data.payload.themeId;
      this.router.navigate(["/themes/" + themeId + "/templates"]);
    })
  );

  constructor(
    private actions$: Actions,
    private templateServe: TemplateService,
    private router: Router
  ) {}
}
