import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { Template } from "@store/template";

@Injectable()
export class TemplateService {
  private readonly endpoint = "/api/template";
  constructor(private http: HttpClient) {}

  getAll(): Observable<Template[]> {
    return this.http.get<Template[]>(this.endpoint);
  }

  get(id): Observable<Template> {
    return this.http.get<Template>(this.endpoint + "/" + id);
  }

  getByThemeId(themeId): Observable<Template[]> {
    return this.http.get<Template[]>(this.endpoint + "/theme/" + themeId).pipe(
      map((templates: Template[]) => {
        return templates.map(template => {
          template.isComponentLoaded = false;
          template.isComponentLoading = false;
          return template;
        });
      })
    );
  }

  create(template: Template): Observable<Template> {
    return this.http.post<Template>(this.endpoint, template);
  }

  update(template: Template): Observable<Template> {
    return this.http.put<Template>(this.endpoint + "/" + template.id, template);
  }

  delete(id: number): Observable<Template> {
    return this.http.delete<Template>(this.endpoint + "/" + id);
  }
}
