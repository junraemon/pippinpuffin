export interface Template {
  name: string;
  themeId: number;
  isComponentLoaded: boolean;
  isComponentLoading: boolean;
  id?: number;
  lastUpdate?: Date;
}
