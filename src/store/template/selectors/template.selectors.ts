import { createSelector, createFeatureSelector } from "@ngrx/store";

import * as fromSection from "@store/section";
import * as fromLayout from "@store/layout";
import * as fromContent from "@store/content";

import * as fromRoot from "@store/router";
import * as fromReducer from "../reducers";
import { Template } from "../models";

export const getTemplateState = createFeatureSelector<
  fromReducer.TemplateState
>("templates");

export const getAllTemplates = createSelector(
  getTemplateState,
  fromReducer.selectAll
);

export const getTemplateEntities = createSelector(
  getTemplateState,
  fromReducer.selectEntities
);

export const getTemplatesByTheme = createSelector(
  getAllTemplates,
  fromRoot.getRouterState,
  (templates, router) => {
    return templates.filter(template => {
      return +router.state.params.themeId === template.themeId;
    });
  }
);

export const getSelectedTemplate = createSelector(
  getTemplateEntities,
  fromRoot.getRouterState,
  (entities, router): Template => {
    return router && entities[router.state.params.templateId];
  }
);

export const getTemplatesLoading = createSelector(
  getTemplateState,
  (state: fromReducer.TemplateState) => state.loading
);

export const getTemplatesLoaded = createSelector(
  getTemplateState,
  (state: fromReducer.TemplateState) => state.loaded
);

export const hasSelectedElement = createSelector(
  fromSection.getSelected,
  fromLayout.getSelected,
  fromContent.getSelected,
  (
    selectedSection: fromSection.TSection,
    selectedLayout: fromLayout.TLayout,
    selectedContent: fromContent.TContent
  ) => !!selectedLayout || !!selectedContent || !!selectedSection
);
