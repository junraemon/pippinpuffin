import { Action } from "@ngrx/store";
import { Template } from "../models";

// delete template
export const DELETE_TEMPLATE = "[Template] Delete";
export const DELETE_TEMPLATE_SUCCESS = "[Template] Delete Success";
export const DELETE_TEMPLATE_FAIL = "[Template] Delete Fail";

export class DeleteTemplate implements Action {
  readonly type = DELETE_TEMPLATE;
  constructor(public payload: number) {}
}

export class DeleteTemplateSuccess implements Action {
  readonly type = DELETE_TEMPLATE_SUCCESS;
  constructor(public payload: Template) {}
}

export class DeleteTemplateFail implements Action {
  readonly type = DELETE_TEMPLATE_FAIL;
  constructor(public payload: number) {}
}

// action types
export type DeleteTemplateAction =
  | DeleteTemplate
  | DeleteTemplateSuccess
  | DeleteTemplateFail;
