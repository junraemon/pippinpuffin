import { Action } from "@ngrx/store";
import { Template } from "../models";

// update template
export const UPDATE_TEMPLATE = "[Template] Update";
export const UPDATE_TEMPLATE_SUCCESS = "[Template] Update Success";
export const UPDATE_TEMPLATE_FAIL = "[Template] Update Fail";

export class UpdateTemplate implements Action {
  readonly type = UPDATE_TEMPLATE;
  constructor(public payload: Template) {}
}

export class UpdateTemplateSuccess implements Action {
  readonly type = UPDATE_TEMPLATE_SUCCESS;
  constructor(public payload: Template) {}
}

export class UpdateTemplateFail implements Action {
  readonly type = UPDATE_TEMPLATE_FAIL;
  constructor(public payload: Template) {}
}

// action types
export type UpdateTemplateAction =
  | UpdateTemplate
  | UpdateTemplateSuccess
  | UpdateTemplateFail;
