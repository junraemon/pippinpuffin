import { Action } from "@ngrx/store";
import { Template } from "../models";

// load templates
export const LOAD_TEMPLATES = "[Templates] Load";
export const LOAD_TEMPLATES_SUCCESS = "[Templates] Load Success";
export const LOAD_TEMPLATES_FAIL = "[Templates] Load Fail";

export class LoadTemplates implements Action {
  readonly type = LOAD_TEMPLATES;
  constructor(public themeId: number) {}
}

export class LoadTemplatesSuccess implements Action {
  readonly type = LOAD_TEMPLATES_SUCCESS;
  constructor(public payload: Template[]) {}
}

export class LoadTemplatesFail implements Action {
  readonly type = LOAD_TEMPLATES_FAIL;
  constructor(public payload: any) {}
}

// action types
export type LoadTemplateAction =
  | LoadTemplates
  | LoadTemplatesSuccess
  | LoadTemplatesFail;
