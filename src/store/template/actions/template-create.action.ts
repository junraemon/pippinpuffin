import { Action } from "@ngrx/store";
import { Template } from "../models";

// create Template
export const CREATE_TEMPLATE = "[Template] Create";
export const CREATE_TEMPLATE_SUCCESS = "[Template] Create Success";
export const CREATE_TEMPLATE_FAIL = "[Template] Create Fail";

export class CreateTemplate implements Action {
  readonly type = CREATE_TEMPLATE;
  constructor(public payload: Template) {}
}

export class CreateTemplateSuccess implements Action {
  readonly type = CREATE_TEMPLATE_SUCCESS;
  constructor(public payload: Template) {}
}

export class CreateTemplateFail implements Action {
  readonly type = CREATE_TEMPLATE_FAIL;
  constructor(public payload: Template) {}
}

// action types
export type CreateTemplateAction =
  | CreateTemplate
  | CreateTemplateSuccess
  | CreateTemplateFail;
