import { LoadTemplateAction } from "./template-load.action";
import { CreateTemplateAction } from "./template-create.action";
import { UpdateTemplateAction } from "./template-update.action";
import { DeleteTemplateAction } from "./template-delete.action";

// action types
export type TemplatesAction =
  | LoadTemplateAction
  | CreateTemplateAction
  | UpdateTemplateAction
  | DeleteTemplateAction;

export * from "./template-load.action";
export * from "./template-create.action";
export * from "./template-update.action";
export * from "./template-delete.action";
