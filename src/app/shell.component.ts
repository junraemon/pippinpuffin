import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import {
  BreakpointObserver,
  BreakpointState,
  Breakpoints
} from "@angular/cdk/layout";
import { MatSidenav } from "@angular/material/sidenav";

import { Observable } from "rxjs";
import { map, tap } from "rxjs/operators";

import { fadeAnimation } from "./router.animation";

export interface MainNavList {
  title: string;
  routerLink: string;
  matIcon: string;
}

@Component({
  selector: "pippin-app-shell",
  templateUrl: "./shell.component.html",
  styleUrls: ["./shell.component.scss"],
  animations: [fadeAnimation]
})
export class ShellComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  @ViewChild(MatSidenav) sidenav: MatSidenav;

  navMenus: MainNavList[] = [
    { title: "Dashboard", routerLink: "/", matIcon: "dashboard" },
    { title: "Themes", routerLink: "/themes", matIcon: "view_list" },
    { title: "Launch Sequence", routerLink: "/", matIcon: "assignment" },
    { title: "Websites", routerLink: "/", matIcon: "web" },
    { title: "Events", routerLink: "/", matIcon: "event_note" },
    { title: "Contacts", routerLink: "/", matIcon: "contacts" },
    { title: "Emails", routerLink: "/", matIcon: "email" },
    { title: "Referrals", routerLink: "/", matIcon: "face" },
    { title: "Forms", routerLink: "/", matIcon: "library_books" },
    { title: "Gallery", routerLink: "/", matIcon: "perm_media" }
  ];

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router
  ) {}

  ngOnInit(): void {}

  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : "";
  }

  navigate(route) {
    this.router.navigate([route]);
    if (this.sidenav.mode === "over") {
      this.sidenav.close();
    }
  }
}
