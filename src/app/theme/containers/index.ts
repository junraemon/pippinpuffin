import { ThemeCustomizeComponent } from "./theme-customize/theme-customize.component";
import { ThemeListComponent } from "./theme-list/theme-list.component";
import { ThemeSettingsComponent } from "./theme-settings/theme-settings.component";

import { TemplateListComponent } from "./template-list/template-list.component";
import { TemplateSettingsComponent } from "./template-settings/template-settings.component";

export const containers: any[] = [
  ThemeSettingsComponent,
  ThemeListComponent,
  ThemeCustomizeComponent,
  TemplateListComponent,
  TemplateSettingsComponent
];

export * from "./theme-list/theme-list.component";
export * from "./theme-settings/theme-settings.component";
export * from "./theme-customize/theme-customize.component";

export * from "./template-list/template-list.component";
export * from "./template-settings/template-settings.component";
