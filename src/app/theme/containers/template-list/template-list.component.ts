import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import * as fromThemeStore from "@store/theme";
import * as fromTemplateStore from "@store/template";
import { Store } from "@ngrx/store";

@Component({
  selector: "pippin-template-list",
  templateUrl: "./template-list.component.html",
  styleUrls: ["./template-list.component.scss"]
})
export class TemplateListComponent implements OnInit {
  theme$: Observable<fromThemeStore.Theme>;
  templates$: Observable<fromTemplateStore.Template[]>;
  constructor(private store: Store<fromThemeStore.ThemeState>) {}

  ngOnInit() {
    this.theme$ = this.store.select(fromThemeStore.getSelectedTheme);
    this.templates$ = this.store.select(fromTemplateStore.getTemplatesByTheme);
  }
}
