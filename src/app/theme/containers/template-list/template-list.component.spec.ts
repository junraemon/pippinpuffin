import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { By } from "@angular/platform-browser";

import {
  MatIconModule,
  MatToolbarModule,
  MatCardModule,
  MatMenuModule
} from "@angular/material";

import { PStoreModule } from "@store/store.module";

import { TemplateListComponent } from "./template-list.component";
import {
  TemplatePreviewListComponent,
  TemplatePreviewComponent
} from "@pippin/theme/components";

xdescribe("TemplateListComponent", () => {
  let component: TemplateListComponent;
  let fixture: ComponentFixture<TemplateListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        MatIconModule,
        MatToolbarModule,
        MatCardModule,
        MatMenuModule,
        PStoreModule.forRoot()
      ],
      declarations: [
        TemplateListComponent,
        TemplatePreviewListComponent,
        TemplatePreviewComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it(`should have a <template-preview-list> directive`, () => {
    const de = fixture.debugElement.query(
      By.directive(TemplatePreviewListComponent)
    );
    expect(de).not.toBeNull();
  });
});
