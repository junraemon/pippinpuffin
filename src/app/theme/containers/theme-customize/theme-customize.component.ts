import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

import * as fromStore from "@store/theme";
import { DomSanitizer } from "@angular/platform-browser";
import { LoaderService } from "@pippin/core";

@Component({
  selector: "pippin-theme-customize",
  templateUrl: "./theme-customize.component.html",
  styleUrls: ["./theme-customize.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThemeCustomizeComponent implements OnInit {
  theme$: Observable<fromStore.Theme>;
  trustedUrl: any;

  @ViewChild("iframe") iframe: ElementRef;

  constructor(
    private store: Store<fromStore.ThemeState>,
    private sanitizer: DomSanitizer,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.theme$ = this.store.select(fromStore.getSelectedTheme).pipe(
      tap((theme: fromStore.Theme) => {
        if (theme) {
          this.loaderService.show();
          this.trustedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
            `/TemplatePage/Preview/${theme.id}`
          );
        }
      })
    );
  }

  onLoad() {
    if (!!this.iframe) {
      this.loaderService.hide();
    }
  }

  onUpdate(theme: fromStore.Theme) {
    this.store.dispatch(new fromStore.CustomizeTheme(theme));
  }
}
