import { PStoreModule } from "@store/store.module";
import { ReactiveFormsModule } from "@angular/forms";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import {
  MatToolbarModule,
  MatIconModule,
  MatFormFieldModule
} from "@angular/material";
import { TemplateSettingsComponent } from "./template-settings.component";
import { TemplateFormSettingsComponent } from "@pippin/theme/components/template-form-settings/template-form-settings.component";

describe("TemplateSettingsComponent", () => {
  let component: TemplateSettingsComponent;
  let fixture: ComponentFixture<TemplateSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        PStoreModule.forRoot(),
        ReactiveFormsModule,
        MatToolbarModule,
        MatIconModule,
        MatFormFieldModule
      ],
      declarations: [TemplateSettingsComponent, TemplateFormSettingsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
