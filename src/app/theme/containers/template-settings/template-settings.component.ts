import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Store } from "@ngrx/store";

import * as fromThemeStore from "@store/theme";
import * as fromTemplateStore from "@store/template";

@Component({
  selector: "pippin-template-settings",
  templateUrl: "./template-settings.component.html",
  styleUrls: ["./template-settings.component.scss"]
})
export class TemplateSettingsComponent implements OnInit {
  theme$: Observable<fromThemeStore.Theme>;
  template$: Observable<fromTemplateStore.Template>;

  constructor(private store: Store<fromThemeStore.ThemeState>) {}

  ngOnInit(): void {
    this.theme$ = this.store.select(fromThemeStore.getSelectedTheme);
    this.template$ = this.store.select(fromTemplateStore.getSelectedTemplate);
  }

  onCreate(payload: fromTemplateStore.Template, themeId: number) {
    this.store.dispatch(
      new fromTemplateStore.CreateTemplate({
        ...payload,
        themeId: themeId
      })
    );
  }

  onUpdate(payload: fromTemplateStore.Template, themeId: number) {
    this.store.dispatch(new fromTemplateStore.UpdateTemplate(payload));
  }

  onDelete(template: fromTemplateStore.Template) {
    const conf = confirm(`Are you sure you want to delete ${template.name}`);
    if (conf) {
      this.store.dispatch(new fromTemplateStore.DeleteTemplate(template.id));
    }
  }
}
