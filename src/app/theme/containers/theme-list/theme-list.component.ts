import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";

import * as fromStore from "@store/theme";
import { Theme } from "@store/theme";

@Component({
  selector: "pippin-theme-list",
  templateUrl: "./theme-list.component.html",
  styleUrls: ["./theme-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThemeListComponent implements OnInit {
  themes$: Observable<Theme[]>;

  constructor(private store: Store<fromStore.ThemeState>) {}

  ngOnInit(): void {
    this.themes$ = this.store.select(fromStore.getAllThemes);
  }
}
