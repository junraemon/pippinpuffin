import { Component, ChangeDetectionStrategy, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";

import * as fromStore from "@store/theme";
import { tap } from "rxjs/operators";

@Component({
  selector: "pippin-theme-settings",
  templateUrl: "./theme-settings.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThemeSettingsComponent implements OnInit {
  theme$: Observable<fromStore.Theme>;

  constructor(private store: Store<fromStore.ThemeState>) {}

  ngOnInit(): void {
    this.theme$ = this.store.select(fromStore.getSelectedTheme);
  }

  onCreate(payload: fromStore.Theme) {
    this.store.dispatch(new fromStore.CreateTheme(payload));
  }

  onUpdate(payload: fromStore.Theme) {
    this.store.dispatch(new fromStore.UpdateTheme(payload));
  }

  onDelete(theme: fromStore.Theme) {
    const conf = confirm(`Are you sure you want to delete ${theme.name}`);
    if (conf) {
      this.store.dispatch(new fromStore.DeleteTheme(theme.id));
    }
  }
}
