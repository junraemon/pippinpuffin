import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// containers
import * as fromContainers from "./containers";

// components
import * as fromComponents from "./components";

// guards and services
import * as fromThemeGuards from "@store/theme/guards";
import * as fromTemplateGuards from "@store/template/guards";

const routes: Routes = [
  {
    path: "",
    canActivate: [fromThemeGuards.ThemesGuard],
    component: fromContainers.ThemeListComponent
  },
  {
    path: "new",
    canActivate: [fromThemeGuards.ThemesGuard],
    component: fromContainers.ThemeSettingsComponent
  },
  {
    path: ":themeId/settings",
    canActivate: [fromThemeGuards.ThemeExistsGuards],
    component: fromContainers.ThemeSettingsComponent
  },
  {
    path: ":themeId/customize",
    canActivate: [fromThemeGuards.ThemeExistsGuards],
    component: fromContainers.ThemeCustomizeComponent
  },
  {
    path: ":themeId/templates",
    canActivate: [
      fromThemeGuards.ThemeExistsGuards,
      fromTemplateGuards.TemplatesGuard
    ],
    component: fromContainers.TemplateListComponent
  },
  {
    path: ":themeId/templates/new",
    canActivate: [fromThemeGuards.ThemeExistsGuards],
    component: fromContainers.TemplateSettingsComponent
  },
  {
    path: ":themeId/templates/:templateId",
    canActivate: [
      fromThemeGuards.ThemeExistsGuards,
      fromTemplateGuards.TemplateExistsGuards
    ],
    component: fromContainers.TemplateSettingsComponent
  }
];

export const ThemeRoutingModule: ModuleWithProviders = RouterModule.forChild(
  routes
);

export * from "./containers";
export * from "./components";
