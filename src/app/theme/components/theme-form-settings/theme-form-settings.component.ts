import {
  Component,
  Output,
  EventEmitter,
  Input,
  OnInit,
  SimpleChanges
} from "@angular/core";
import {
  FormBuilder,
  Validators,
  FormControl,
  FormGroup
} from "@angular/forms";

import { Theme } from "@store/theme";

@Component({
  selector: "pippin-theme-form-settings",
  templateUrl: "./theme-form-settings.component.html",
  styleUrls: ["./theme-form-settings.component.scss"]
})
export class ThemeFormSettingsComponent implements OnInit {
  exists = false;

  @Input() theme: Theme;

  @Output() create = new EventEmitter();
  @Output() update = new EventEmitter();
  @Output() delete = new EventEmitter();

  form = this.fb.group({
    name: ["", Validators.required],
    description: ["", Validators.required]
  });

  constructor(private fb: FormBuilder) {}

  get name() {
    return this.form.get("name") as FormControl;
  }

  get description() {
    return this.form.get("description") as FormControl;
  }

  ngOnInit(): void {
    if (this.theme && this.theme.id) {
      this.exists = true;
      this.form.patchValue(this.theme);
    }
  }

  createTheme(form: FormGroup) {
    const { value, valid } = form;
    if (valid) {
      this.create.emit(value);
    }
  }

  updateTheme(form: FormGroup) {
    const { value, valid } = form;
    if (valid) {
      this.update.emit({ ...this.theme, ...value });
    }
  }

  deleteTheme() {
    this.delete.emit(this.theme);
  }
}
