import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";

import { Theme } from "@store/theme";

@Component({
  selector: "pippin-theme-preview",
  templateUrl: "./theme-preview.component.html",
  styleUrls: ["./theme-preview.component.scss"]
})
export class ThemePreviewComponent {
  @Input() theme: Theme;

  get id() {
    return this.theme.id;
  }

  get name() {
    return this.theme.name;
  }

  get description() {
    return this.theme.description;
  }

  get lastUpdate() {
    return this.theme.lastUpdate;
  }
}
