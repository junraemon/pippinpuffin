import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {
  FormBuilder,
  Validators,
  FormControl,
  FormGroup
} from "@angular/forms";

import { Theme } from "@store/theme";

@Component({
  selector: "pippin-theme-form-customize",
  templateUrl: "./theme-form-customize.component.html",
  styleUrls: ["./theme-form-customize.component.scss"]
})
export class ThemeFormCustomizeComponent implements OnInit {
  @Input() theme: Theme;
  @Output() update = new EventEmitter();

  form = this.fb.group({
    customCss: ["", Validators.required]
  });

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form.patchValue(this.theme);
  }

  get customCss() {
    return this.form.get("customCss") as FormControl;
  }

  updateTheme(form: FormGroup) {
    const { value, valid } = form;
    if (valid) {
      this.update.emit({ ...this.theme, ...value });
    }
  }
}
