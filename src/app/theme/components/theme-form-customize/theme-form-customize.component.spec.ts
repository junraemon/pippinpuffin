import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { ReactiveFormsModule } from "@angular/forms";

import { CodemirrorModule } from "@ctrl/ngx-codemirror";
import { ThemeFormCustomizeComponent } from "./theme-form-customize.component";

xdescribe("ThemeFormCustomizeComponent", () => {
  let component: ThemeFormCustomizeComponent;
  let fixture: ComponentFixture<ThemeFormCustomizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, CodemirrorModule],
      declarations: [ThemeFormCustomizeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemeFormCustomizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
