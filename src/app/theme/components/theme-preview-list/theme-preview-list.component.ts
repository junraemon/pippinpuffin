import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

import { Theme } from "@store/theme";

@Component({
  selector: "pippin-theme-preview-list",
  templateUrl: "./theme-preview-list.component.html",
  styleUrls: ["./theme-preview-list.component.scss"]
})
export class ThemePreviewListComponent {
  @Input() themes: Theme[];
}
