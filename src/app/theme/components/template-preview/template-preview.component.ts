import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

import { Template } from "@store/template";

@Component({
  selector: "pippin-template-preview",
  templateUrl: "./template-preview.component.html",
  styleUrls: ["./template-preview.component.scss"]
})
export class TemplatePreviewComponent {
  @Input() template: Template;

  get id() {
    return this.template.id;
  }

  get themeId() {
    return this.template.themeId;
  }

  get name() {
    return this.template.name;
  }

  get lastUpdate() {
    return this.template.lastUpdate;
  }
}
