import { ThemePreviewListComponent } from "./theme-preview-list/theme-preview-list.component";
import { ThemePreviewComponent } from "./theme-preview/theme-preview.component";
import { ThemeFormSettingsComponent } from "./theme-form-settings/theme-form-settings.component";
import { ThemeFormCustomizeComponent } from "./theme-form-customize/theme-form-customize.component";

import { TemplatePreviewListComponent } from "./template-preview-list/template-preview-list.component";
import { TemplatePreviewComponent } from "./template-preview/template-preview.component";
import { TemplateFormSettingsComponent } from "./template-form-settings/template-form-settings.component";

export const components: any[] = [
  ThemePreviewListComponent,
  ThemePreviewComponent,
  ThemeFormSettingsComponent,
  ThemeFormCustomizeComponent,
  TemplatePreviewListComponent,
  TemplatePreviewComponent,
  TemplateFormSettingsComponent
];

export * from "./theme-preview-list/theme-preview-list.component";
export * from "./theme-preview/theme-preview.component";
export * from "./theme-form-settings/theme-form-settings.component";

export * from "./template-preview-list/template-preview-list.component";
export * from "./template-preview/template-preview.component";
export * from "./theme-form-customize/theme-form-customize.component";
