import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { By } from "@angular/platform-browser";

import { MatCardModule, MatMenuModule, MatIconModule } from "@angular/material";

import { TemplatePreviewListComponent } from "./template-preview-list.component";
import { TemplatePreviewComponent } from "@pippin/theme/components";
import { Template } from "@store/template";

describe("TemplatePreviewListComponent", () => {
  let component: TemplatePreviewListComponent;
  let fixture: ComponentFixture<TemplatePreviewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        BrowserAnimationsModule,
        MatCardModule,
        MatMenuModule,
        MatIconModule
      ],
      declarations: [TemplatePreviewListComponent, TemplatePreviewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplatePreviewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it(`should have a <template-preview> directive`, () => {
    component.templates = [
      {
        name: "Template 1",
        themeId: 100,
        isComponentLoaded: true,
        isComponentLoading: true
      }
    ];

    fixture.detectChanges();

    const de = fixture.debugElement.query(
      By.directive(TemplatePreviewComponent)
    );
    expect(de).not.toBeNull();
  });
});
