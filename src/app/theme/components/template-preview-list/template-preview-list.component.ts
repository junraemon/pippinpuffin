import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

import { Template } from "@store/template";

@Component({
  selector: "pippin-template-preview-list",
  templateUrl: "./template-preview-list.component.html",
  styleUrls: ["./template-preview-list.component.scss"]
})
export class TemplatePreviewListComponent {
  @Input() templates: Template[];

  constructor() {}
}
