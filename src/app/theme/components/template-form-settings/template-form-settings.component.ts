import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {
  Validators,
  FormBuilder,
  FormControl,
  FormGroup
} from "@angular/forms";

import { Template } from "@store/template";

@Component({
  selector: "pippin-template-form-settings",
  templateUrl: "./template-form-settings.component.html",
  styleUrls: ["./template-form-settings.component.scss"]
})
export class TemplateFormSettingsComponent implements OnInit {
  exists = false;

  @Input() template: Template;

  @Output() create = new EventEmitter();
  @Output() update = new EventEmitter();
  @Output() delete = new EventEmitter();

  form = this.fb.group({
    name: ["", Validators.required]
  });

  constructor(private fb: FormBuilder) {}

  get name() {
    return this.form.get("name") as FormControl;
  }

  ngOnInit(): void {
    if (this.template && this.template.id) {
      this.exists = true;
      this.form.patchValue(this.template);
    }
  }

  createTemplate(form: FormGroup) {
    const { value, valid } = form;
    if (valid) {
      this.create.emit(value);
    }
  }

  updateTemplate(form: FormGroup) {
    const { value, valid } = form;
    if (valid) {
      this.update.emit({ ...this.template, ...value });
    }
  }

  deleteTemplate() {
    this.delete.emit(this.template);
  }
}
