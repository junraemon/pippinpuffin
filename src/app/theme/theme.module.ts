import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";

import {
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatButtonModule,
  MatIconModule,
  MatMenuModule,
  MatToolbarModule,
  MatSidenavModule
} from "@angular/material";

// codemirror module
import { CodemirrorModule } from "@ctrl/ngx-codemirror";
import "codemirror/mode/sass/sass";

import * as themeRouting from "@pippin/theme/theme.routing";

// shared
import { SharedModule } from "@pippin/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    CodemirrorModule,
    SharedModule,
    themeRouting.ThemeRoutingModule
  ],
  declarations: [...themeRouting.containers, ...themeRouting.components],
  exports: [...themeRouting.containers, ...themeRouting.components]
})
export class ThemeModule {}
