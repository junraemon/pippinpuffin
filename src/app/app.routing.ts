import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ShellComponent } from "@pippin/shell.component";

const routes: Routes = [
  {
    path: "",
    component: ShellComponent,
    children: [
      {
        path: "",
        loadChildren: "@pippin/home/home.module#HomeModule"
      },
      {
        path: "themes",
        loadChildren: "@pippin/theme/theme.module#ThemeModule"
      },
      {
        path: "editor",
        loadChildren: "@pippin/editor/editor.module#PEditorModule"
      }
    ]
  },
  { path: "login", loadChildren: "@pippin/auth/auth.module#AuthModule" },
  { path: "**", redirectTo: "/" }
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(
  routes
);
