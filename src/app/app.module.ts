import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { ServiceWorkerModule } from "@angular/service-worker";
import { SortablejsModule, SortablejsOptions } from "angular-sortablejs";

import {
  MatSidenavModule,
  MatListModule,
  MatIconModule,
  MatToolbarModule
} from "@angular/material";

import { AppRoutingModule } from "@pippin/app.routing";
import { CoreModule } from "@pippin/core/core.module";
import { SharedModule } from "@pippin/shared/shared.module";

import { AppComponent } from "@pippin/app.component";
import { ShellComponent } from "@pippin/shell.component";
import { PStoreModule } from "@store/store.module";

import { environment } from "../environments/environment";

const sortableOptions: SortablejsOptions = {
  animation: 150,
  handle: ".sortable-item-handler",
  draggable: ".sortable-item",
  dragClass: "sortable-drag",
  ghostClass: "placeholder"
};

@NgModule({
  declarations: [AppComponent, ShellComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register("/ngsw-worker.js", {
      enabled: environment.production
    }),
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    SharedModule,
    CoreModule,
    AppRoutingModule,
    PStoreModule.forRoot(),
    SortablejsModule.forRoot(sortableOptions)
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
