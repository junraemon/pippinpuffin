import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import {
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatCardModule,
  MatButtonModule
} from "@angular/material";

import * as routing from "./auth.routing";
import * as fromComponents from "./components";

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    routing.AuthRoutingModule
  ],
  declarations: [...routing.containers, ...fromComponents.components]
})
export class AuthModule {}
