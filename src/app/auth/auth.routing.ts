import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import * as fromComponents from "./containers";

const routes: Routes = [
  {
    path: "",
    component: fromComponents.LoginComponent
  }
];

export const AuthRoutingModule: ModuleWithProviders = RouterModule.forChild(
  routes
);

export * from "./containers";
