import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { By } from "@angular/platform-browser";

import {
  MatFormFieldModule,
  MatCardModule,
  MatIconModule,
  MatInputModule
} from "@angular/material";

import { LoginComponent } from "./login.component";
import { LoginFormComponent } from "@pippin/auth/components";

describe("LoginComponent", () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatCardModule,
        MatIconModule,
        MatInputModule
      ],
      declarations: [LoginComponent, LoginFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it(`should have a <pippin-login-form> directive`, () => {
    const de = fixture.debugElement.query(By.directive(LoginFormComponent));
    expect(de).not.toBeNull();
  });
});
