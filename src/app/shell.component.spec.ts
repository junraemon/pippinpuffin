import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { TestBed, async, ComponentFixture } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { By } from "@angular/platform-browser";
import { RouterOutlet } from "@angular/router";

import {
  MatSidenavModule,
  MatListModule,
  MatIconModule,
  MatMenuModule,
  MatToolbarModule
} from "@angular/material";

import { ShellComponent } from "./shell.component";
import { NavMenuComponent } from "@pippin/core";

describe("ShellComponent", () => {
  let fixture: ComponentFixture<ShellComponent>;
  let component: ShellComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        BrowserAnimationsModule,
        MatSidenavModule,
        MatListModule,
        MatIconModule,
        MatMenuModule,
        MatToolbarModule
      ],
      declarations: [ShellComponent, NavMenuComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShellComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

  it("should create the ShellComponent", async(() => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have a <pippin-nav-menu> directive`, () => {
    const de = fixture.debugElement.query(By.directive(NavMenuComponent));
    expect(de).not.toBeNull();
  });
});
