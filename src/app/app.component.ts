import { Component } from "@angular/core";
import {
  Router,
  RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from "@angular/router";
import { Observable } from "rxjs";

import { LoaderService } from "./core";

@Component({
  selector: "pippin-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  currentRoute: any;

  constructor(private router: Router, private loaderService: LoaderService) {
    router.events.subscribe((event: any) => {
      this.currentRoute = event.url;
      this.navigationInterceptor(event);
    });
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loaderService.show();
    }

    if (
      event instanceof NavigationEnd ||
      event instanceof NavigationCancel ||
      event instanceof NavigationError
    ) {
      this.loaderService.hide();
    }
  }
}
