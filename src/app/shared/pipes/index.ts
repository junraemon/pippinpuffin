import { TruncatePipe } from "./truncate.pipe";
import { SafeHtmlPipe } from "./safehtml.pipe";

export const pipes: any[] = [TruncatePipe, SafeHtmlPipe];

export * from "./truncate.pipe";
export * from "./safehtml.pipe";
