import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import * as fromComponents from "./components";
import * as fromPipes from "./pipes";
import * as fromServices from "./services";

@NgModule({
  imports: [CommonModule],
  exports: [...fromComponents.components, ...fromPipes.pipes],
  declarations: [...fromComponents.components, ...fromPipes.pipes],
  providers: [...fromServices.services]
})
export class SharedModule {}
