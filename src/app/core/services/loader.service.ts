import { Observable ,  BehaviorSubject } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable()
export class LoaderService {
  _loader: BehaviorSubject<boolean>;
  public loader: Observable<boolean>;

  constructor() {
    this._loader = new BehaviorSubject(false);
    this.loader = this._loader.asObservable();
  }

  show() {
    this._loader.next(true);
  }

  hide() {
    this._loader.next(false);
  }
}
