import { NavMenuComponent } from "./nav-menu/nav-menu.component";
import { LoaderComponent } from "./loader/loader.component";

export const components: any[] = [NavMenuComponent, LoaderComponent];

export * from "./nav-menu/nav-menu.component";
export * from "./loader/loader.component";
