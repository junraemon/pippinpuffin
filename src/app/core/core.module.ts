import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import {
  MatToolbarModule,
  MatIconModule,
  MatMenuModule,
  MatButtonModule
} from "@angular/material";

import * as fromComponents from "./components";
import * as fromServices from "./services";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule
  ],
  declarations: [...fromComponents.components],
  exports: [...fromComponents.components],
  providers: [...fromServices.services]
})
export class CoreModule {}
