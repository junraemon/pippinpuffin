import { Component, OnInit, OnDestroy } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { Store } from "@ngrx/store";

import * as fromContent from "@store/content";
import { FormGroup, FormBuilder } from "@angular/forms";
import { tap, distinctUntilChanged } from "rxjs/operators";

@Component({
  selector: "pippin-email-button-form",
  templateUrl: "./email-button-form.component.html",
  styleUrls: ["./email-button-form.component.scss"]
})
export class EmailButtonFormComponent implements OnInit, OnDestroy {
  selectedContent: fromContent.TContent;

  form: FormGroup = this.fb.group({
    link: [""],
    caption: [""],
    alignment: [""],
    size: [""],
    type: [""],
    corners: [""],
    expanded: [""]
  });

  storeSubs$: Subscription;
  formSubs$: Subscription;

  constructor(
    private store: Store<fromContent.ContentState>,
    private fb: FormBuilder
  ) {
    this.storeSubs$ = this.store
      .select(fromContent.getSelected)
      .pipe(
        tap(result => {
          this.selectedContent = result;

          if (result) {
            const buttonData: fromContent.ContentButton = result.data;

            this.form.reset({
              link: buttonData.link,
              caption: buttonData.caption,
              alignment: buttonData.alignment,
              size: buttonData.size,
              type: buttonData.type,
              corners: buttonData.corners,
              expanded: buttonData.expanded
            });
          }
        })
      )
      .subscribe();
  }

  get contentType() {
    return fromContent.ContentType;
  }

  ngOnInit(): void {
    this.formSubs$ = this.form.valueChanges.subscribe(result => {
      if (!!this.selectedContent) {
        this.store.dispatch(
          new fromContent.UpdateSelectedContent({
            ...this.selectedContent,
            data: {
              ...this.selectedContent.data,
              ...result
            }
          })
        );
      }
    });
  }

  submit() {
    this.store.dispatch(
      new fromContent.UpdateButton({
        buttonId: this.selectedContent.data.id,
        button: this.form.value
      })
    );
  }

  ngOnDestroy(): void {
    this.storeSubs$.unsubscribe();
    this.formSubs$.unsubscribe();
  }
}
