import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { RouterTestingModule } from "@angular/router/testing";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { ReactiveFormsModule } from "@angular/forms";
import {
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule
} from "@angular/material";

import { EmailButtonFormComponent } from "./email-button-form.component";
import { PStoreModule } from "@store/store.module";

describe("EmailButtonFormComponent", () => {
  let component: EmailButtonFormComponent;
  let fixture: ComponentFixture<EmailButtonFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        PStoreModule.forRoot(),
        RouterTestingModule.withRoutes([]),
        NoopAnimationsModule
      ],
      declarations: [EmailButtonFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailButtonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
