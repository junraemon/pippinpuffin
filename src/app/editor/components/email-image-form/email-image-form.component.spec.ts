import { PStoreModule } from "@store/store.module";
import { RouterTestingModule } from "@angular/router/testing";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { ReactiveFormsModule } from "@angular/forms";
import {
  MatInputModule,
  MatFormFieldModule,
  MatSelectModule
} from "@angular/material";

import { EmailImageFormComponent } from "./email-image-form.component";

describe("EmailImageFormComponent", () => {
  let component: EmailImageFormComponent;
  let fixture: ComponentFixture<EmailImageFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        PStoreModule.forRoot(),
        RouterTestingModule.withRoutes([]),
        NoopAnimationsModule
      ],
      declarations: [EmailImageFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailImageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
