import { Component, OnDestroy, OnInit } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { Store } from "@ngrx/store";

import * as fromContent from "@store/content";
import { FormGroup, FormBuilder } from "@angular/forms";
import { tap, distinctUntilChanged } from "rxjs/operators";

@Component({
  selector: "pippin-email-image-form",
  templateUrl: "./email-image-form.component.html",
  styleUrls: ["./email-image-form.component.scss"]
})
export class EmailImageFormComponent implements OnInit, OnDestroy {
  selectedContent: fromContent.TContent;

  form: FormGroup = this.fb.group({
    url: [""],
    alignment: [""],
    width: [""],
    height: [""]
  });

  storeSubs$: Subscription;
  formSubs$: Subscription;

  constructor(
    private store: Store<fromContent.ContentState>,
    private fb: FormBuilder
  ) {
    this.storeSubs$ = this.store
      .select(fromContent.getSelected)
      .pipe(
        tap(result => {
          this.selectedContent = result;

          if (result) {
            const { url, alignment, width, height } = result.data;
            this.form.reset({ url, alignment, width, height });
          }
        })
      )
      .subscribe();
  }

  get contentType() {
    return fromContent.ContentType;
  }

  ngOnInit(): void {
    this.formSubs$ = this.form.valueChanges.subscribe(result => {
      if (!!this.selectedContent) {
        this.store.dispatch(
          new fromContent.UpdateSelectedContent({
            ...this.selectedContent,
            data: {
              ...this.selectedContent.data,
              ...result
            }
          })
        );
      }
    });
  }

  submit() {
    this.store.dispatch(
      new fromContent.UpdateImage({
        imageId: this.selectedContent.data.id,
        image: this.form.value
      })
    );
  }

  ngOnDestroy(): void {
    this.storeSubs$.unsubscribe();
    this.formSubs$.unsubscribe();
  }
}
