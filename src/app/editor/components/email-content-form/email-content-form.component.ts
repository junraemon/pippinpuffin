import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Store } from "@ngrx/store";
import * as fromContent from "@store/content";
import { Observable, Subscription } from "rxjs";
import { tap } from "rxjs/operators";

@Component({
  selector: "pippin-email-content-form",
  templateUrl: "./email-content-form.component.html",
  styleUrls: ["./email-content-form.component.scss"]
})
export class EmailContentFormComponent implements OnInit, OnDestroy {
  selectedContent: any;
  selectedContent$: Observable<fromContent.TContent>;
  toggleColorPicker = false;
  selectedColor: string;

  form: FormGroup = this.fb.group({
    backgroundColor: [""],
    spacing: [""],
    calloutStyle: [""],
    customClass: [""]
  });

  storeSubs$: Subscription;
  formSubs$: Subscription;

  constructor(
    private store: Store<fromContent.ContentState>,
    private fb: FormBuilder
  ) {
    this.storeSubs$ = this.store
      .select(fromContent.getSelected)
      .pipe(
        tap(result => {
          this.selectedContent = result;

          if (result) {
            const contentData: fromContent.TContent = result;

            this.form.reset({
              backgroundColor: contentData.backgroundColor,
              spacing: contentData.spacing,
              calloutStyle: contentData.calloutStyle + "",
              customClass: contentData.customClass
            });

            this.selectedColor = contentData.backgroundColor;
          }
        })
      )
      .subscribe();
  }

  get contentType() {
    return fromContent.ContentType;
  }

  ngOnInit(): void {
    this.formSubs$ = this.form.valueChanges.subscribe(result => {
      if (!!this.selectedContent) {
        this.selectedColor = result.backgroundColor;
        this.store.dispatch(
          new fromContent.UpdateSelectedContent({
            ...this.selectedContent,
            ...result
          })
        );
      }
    });
  }

  onBackgroundColorChange(color: string) {
    this.form.controls["backgroundColor"].patchValue(color);
  }

  togglePicker(event: any) {
    this.toggleColorPicker = !this.toggleColorPicker;
  }

  resetBackgroundColor() {
    this.form.controls["backgroundColor"].patchValue("");
  }

  submit() {
    this.store.dispatch(
      new fromContent.SaveContent({
        id: this.selectedContent.id,
        content: this.form.value
      })
    );
  }

  ngOnDestroy(): void {
    this.storeSubs$.unsubscribe();
    this.formSubs$.unsubscribe();
  }
}
