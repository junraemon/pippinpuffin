import { RouterTestingModule } from "@angular/router/testing";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule } from "@angular/forms";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import {
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatSelectModule
} from "@angular/material";
import { PStoreModule } from "@store/store.module";
import { MccColorPickerModule } from "material-community-components";

import { EmailContentFormComponent } from "./email-content-form.component";

describe("EmailContentFormComponent", () => {
  let component: EmailContentFormComponent;
  let fixture: ComponentFixture<EmailContentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatIconModule,
        PStoreModule.forRoot(),
        RouterTestingModule.withRoutes([]),
        NoopAnimationsModule,
        MccColorPickerModule.forRoot({
          empty_color: "none"
        })
      ],
      declarations: [EmailContentFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailContentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
