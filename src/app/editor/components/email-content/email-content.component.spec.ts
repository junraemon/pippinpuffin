import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientModule } from "@angular/common/http";

import { SortablejsDirective } from "angular-sortablejs";
import { PStoreModule } from "@store/store.module";

import {
  FoundationTextComponent,
  FoundationImageComponent,
  FoundationButtonComponent
} from "@lib/foundation/components";

import { EmailContentComponent } from "./email-content.component";

xdescribe("EmailContentComponent", () => {
  let component: EmailContentComponent;
  let fixture: ComponentFixture<EmailContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        HttpClientModule,
        PStoreModule.forRoot()
      ],
      declarations: [
        SortablejsDirective,
        EmailContentComponent,
        FoundationTextComponent,
        FoundationImageComponent,
        FoundationButtonComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", async () => {
    expect(component).toBeTruthy();
  });
});
