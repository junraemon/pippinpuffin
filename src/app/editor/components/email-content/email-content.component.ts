import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  NgZone,
  AfterContentChecked
} from "@angular/core";
import { SortablejsOptions } from "angular-sortablejs";
import { Observable } from "rxjs";
import { Store } from "@ngrx/store";

import * as fromSection from "@store/section";
import * as fromLayout from "@store/layout";
import * as fromContent from "@store/content";
import { tap } from "rxjs/operators";

@Component({
  selector: "pippin-email-content",
  templateUrl: "./email-content.component.html",
  styleUrls: ["./email-content.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class EmailContentComponent implements OnInit, AfterContentChecked {
  @Input()
  column: fromLayout.TColumn;
  contents$: Observable<fromContent.TContent[]>;
  selectedContent$: Observable<fromContent.TContent>;
  components: fromContent.TContent[];

  sortableOptions: SortablejsOptions = {
    group: "contents",
    onUpdate: e => this.onUpdate(e),
    onAdd: e => this.onAdd(e)
  };

  constructor(
    private fromSectionStore: Store<fromSection.SectionState>,
    private fromLayoutStore: Store<fromLayout.LayoutState>,
    private fromContentStore: Store<fromContent.ContentState>,
    private _ngZone: NgZone
  ) {}

  ngOnInit() {
    this.contents$ = this.fromContentStore
      .select(fromContent.getContentsByColumnId(this.column.id))
      .pipe(
        tap(contents => {
          this.components = contents;
          if (contents.length === 0) {
            this.fromContentStore.dispatch(
              new fromContent.LoadContents(this.column.id)
            );
          }
        })
      );
  }

  ngAfterContentChecked(): void {
    this.selectedContent$ = this.fromContentStore.select(
      fromContent.getCopyOfSelected
    );
  }

  get getType() {
    return fromContent.ContentType;
  }

  onAdd(evt) {
    this._ngZone.run(() => {
      if (!!evt.item.newContent) {
        const { type, typeId } = evt.item.newContent;
        this.fromContentStore.dispatch(
          new fromContent.AddContent({
            columnId: evt.to["column-id"],
            type: type,
            typeId: typeId,
            position: evt.newIndex
          })
        );
      } else {
        this.fromContentStore.dispatch(
          new fromContent.SortContent({
            columnId: evt.to["column-id"],
            contentIds: this.mapper(this.components),
            movedContentId: evt.item["content-id"]
          })
        );
      }
    });
  }

  onUpdate(evt) {
    this._ngZone.run(() => {
      this.fromContentStore.dispatch(
        new fromContent.SortContent({
          columnId: evt.to["column-id"],
          contentIds: this.mapper(this.components),
          movedContentId: 0
        })
      );
    });
  }

  selectContent(content: fromContent.TContent) {
    if (!content.isSelected) {
      this.fromSectionStore.dispatch(new fromSection.ClearSelectedSection());
      this.fromLayoutStore.dispatch(new fromLayout.ClearSelectedLayout());
      this.fromContentStore.dispatch(new fromContent.ClearSelectedContent());
      this.fromContentStore.dispatch(
        new fromContent.SetSelectedContent(content)
      );
    }
  }

  onDelete(contentId: number) {
    this.fromContentStore.dispatch(new fromContent.ClearSelectedContent());
    this.fromContentStore.dispatch(new fromContent.DeleteContent(contentId));
  }

  mapper(items: fromContent.TContent[]): number[] {
    return items.map(item => {
      return item.id;
    });
  }
}
