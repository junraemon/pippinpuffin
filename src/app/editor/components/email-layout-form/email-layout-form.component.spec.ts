import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailLayoutFormComponent } from './email-layout-form.component';

describe('EmailLayoutFormComponent', () => {
  let component: EmailLayoutFormComponent;
  let fixture: ComponentFixture<EmailLayoutFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailLayoutFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailLayoutFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
