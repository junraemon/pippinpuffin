import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { RouterTestingModule } from "@angular/router/testing";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { ReactiveFormsModule } from "@angular/forms";
import {
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule
} from "@angular/material";
import { EditorModule } from "@tinymce/tinymce-angular";

import { EmailTextFormComponent } from "./email-text-form.component";
import { PStoreModule } from "@store/store.module";

describe("EmailTextFormComponent", () => {
  let component: EmailTextFormComponent;
  let fixture: ComponentFixture<EmailTextFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        EditorModule,
        PStoreModule.forRoot(),
        RouterTestingModule.withRoutes([]),
        NoopAnimationsModule
      ],
      declarations: [EmailTextFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTextFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
