import { Subscription, Observable } from "rxjs";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Store } from "@ngrx/store";
import { tap, distinctUntilChanged } from "rxjs/operators";

import * as fromContent from "@store/content";
@Component({
  selector: "pippin-email-text-form",
  templateUrl: "./email-text-form.component.html",
  styleUrls: ["./email-text-form.component.scss"]
})
export class EmailTextFormComponent implements OnInit, OnDestroy {
  selectedContent: fromContent.TContent;

  form: FormGroup = this.fb.group({
    content: [""]
  });

  storeSubs$: Subscription;
  formSubs$: Subscription;

  constructor(
    private store: Store<fromContent.ContentState>,
    private fb: FormBuilder
  ) {
    this.storeSubs$ = this.store
      .select(fromContent.getSelected)
      .pipe(
        tap(result => {
          this.selectedContent = result;
          if (result) {
            this.form.reset({
              content: result.data.content
            });
          }
        })
      )
      .subscribe();
  }

  get contentType() {
    return fromContent.ContentType;
  }

  get tinyMCESettings() {
    return {
      plugins: [
        "advlist autolink lists link charmap print preview anchor",
        "searchreplace visualblocks code fullscreen textcolor",
        "insertdatetime media table contextmenu paste colorpicker"
      ],
      toolbar1:
        "undo redo styleselect bold italic underline strikethrough | link",
      toolbar2:
        "alignleft aligncenter alignright alignjustify | bullist numlist | forecolor backcolor",
      branding: false,
      menubar: false,
      statusbar: false,
      forced_root_blocks: false,
      force_br_newlines: true,
      force_p_newlines: false,
      convert_newlines_to_brs: true,
      skin_url: "/assets/tinymce/skins/lightgray",
      theme_url: "/assets/tinymce/themes/modern/theme.min.js"
    };
  }

  ngOnInit(): void {
    this.formSubs$ = this.form.valueChanges
      .pipe(distinctUntilChanged((x, y) => x.content === y.content))
      .subscribe(result => {
        if (!!this.selectedContent) {
          this.store.dispatch(
            new fromContent.UpdateSelectedContent({
              ...this.selectedContent,
              data: {
                ...this.selectedContent.data,
                content: result.content
              }
            })
          );
        }
      });
  }

  submit() {
    this.store.dispatch(
      new fromContent.UpdateText({
        textId: this.selectedContent.data.id,
        text: this.form.value
      })
    );
  }

  ngOnDestroy(): void {
    this.storeSubs$.unsubscribe();
    this.formSubs$.unsubscribe();
  }
}
