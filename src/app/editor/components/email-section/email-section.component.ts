import {
  Component,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  NgZone,
  Input,
  OnInit
} from "@angular/core";
import { SortablejsOptions } from "angular-sortablejs/dist";
import { Observable } from "rxjs";
import { Store } from "@ngrx/store";

import * as fromSection from "@store/section";
import * as fromLayout from "@store/layout";
import * as fromContent from "@store/content";
import { tap } from "rxjs/operators";

@Component({
  selector: "pippin-email-section",
  templateUrl: "./email-section.component.html",
  styleUrls: ["./email-section.component.scss"],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmailSectionComponent implements OnInit {
  @Input()
  templateId: number;
  sections$: Observable<fromSection.TSection[]>;
  components: fromSection.TSection[];

  sortableOptions: SortablejsOptions = {
    group: "sections",
    onAdd: e => this.onAdd(e),
    onUpdate: e => this.onUpdate(e)
  };

  constructor(
    private fromSectionStore: Store<fromSection.SectionState>,
    private fromLayoutStore: Store<fromLayout.LayoutState>,
    private fromContentStore: Store<fromContent.ContentState>,
    private _ngZone: NgZone
  ) {}

  ngOnInit(): void {
    this.sections$ = this.fromSectionStore
      .select(fromSection.getSectionsByTemplate)
      .pipe(
        tap(sections => {
          if (sections) {
            this.components = sections;
          }
        })
      );
  }

  onAdd(evt) {
    this._ngZone.run(() => {
      this.fromSectionStore.dispatch(
        new fromSection.AddSection(this.templateId, {
          type: 1,
          position: evt.newIndex
        })
      );
    });
  }

  onUpdate(evt) {
    this._ngZone.run(() => {
      this.fromSectionStore.dispatch(
        new fromSection.SortSection(
          this.templateId,
          this.mapper(this.components)
        )
      );
    });
  }

  onSelect(section: fromSection.TSection) {
    if (!section.isSelected) {
      this.fromSectionStore.dispatch(new fromSection.ClearSelectedSection());
      this.fromLayoutStore.dispatch(new fromLayout.ClearSelectedLayout());
      this.fromContentStore.dispatch(new fromContent.ClearSelectedContent());
      this.fromSectionStore.dispatch(
        new fromSection.SetSelectedSection(section)
      );
    }
  }

  onDelete(sectionId: number) {
    this.fromSectionStore.dispatch(new fromSection.DeleteSection(sectionId));
  }

  mapper(items: fromSection.TSection[]): number[] {
    return items.map(item => {
      return item.id;
    });
  }
}
