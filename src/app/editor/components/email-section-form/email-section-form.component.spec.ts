import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailSectionFormComponent } from './email-section-form.component';

describe('EmailSectionFormComponent', () => {
  let component: EmailSectionFormComponent;
  let fixture: ComponentFixture<EmailSectionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailSectionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailSectionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
