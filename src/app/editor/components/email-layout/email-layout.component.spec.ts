import { SortablejsModule } from "angular-sortablejs";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { EmailLayoutComponent } from "./email-layout.component";

xdescribe("EmailLayoutComponent", () => {
  let component: EmailLayoutComponent;
  let fixture: ComponentFixture<EmailLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SortablejsModule],
      declarations: [EmailLayoutComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
