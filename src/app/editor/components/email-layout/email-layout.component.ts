import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  NgZone,
  ChangeDetectionStrategy
} from "@angular/core";
import { SortablejsOptions } from "angular-sortablejs";
import { Observable } from "rxjs";
import { Store } from "@ngrx/store";

import * as fromSection from "@store/section";
import * as fromLayout from "@store/layout";
import * as fromContent from "@store/content";
import { tap } from "rxjs/operators";

@Component({
  selector: "pippin-email-layout",
  templateUrl: "./email-layout.component.html",
  styleUrls: ["./email-layout.component.scss"],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmailLayoutComponent implements OnInit {
  @Input()
  section: fromSection.TSection;
  layouts$: Observable<fromLayout.TLayout[]>;
  components: fromLayout.TLayout[];

  sortableOptions: SortablejsOptions = {
    group: "layouts",
    onUpdate: e => this.onUpdate(e),
    onAdd: e => this.onAdd(e)
  };

  constructor(
    private fromSectionStore: Store<fromSection.SectionState>,
    private fromLayoutStore: Store<fromLayout.LayoutState>,
    private fromContentStore: Store<fromContent.ContentState>,
    private _ngZone: NgZone
  ) {}

  ngOnInit() {
    this.layouts$ = this.fromLayoutStore
      .select(fromLayout.getLayoutsBySectionId(this.section.id))
      .pipe(
        tap(layouts => {
          this.components = layouts;
          if (layouts.length === 0) {
            this.fromLayoutStore.dispatch(
              new fromLayout.LoadLayouts(this.section.id)
            );
          }
        })
      );
  }

  onAdd(evt) {
    this._ngZone.run(() => {
      if (!!evt.item["new-layout"]) {
        const layout = this.addLayout(evt);
        this.fromLayoutStore.dispatch(new fromLayout.AddLayout(layout));
      } else {
        const layout = this.sortLayout(evt);
        this.fromLayoutStore.dispatch(new fromLayout.SortLayout(layout));
      }
    });
  }

  onUpdate(evt) {
    this._ngZone.run(() => {
      const layout = this.sortLayout(evt);
      this.fromLayoutStore.dispatch(new fromLayout.SortLayout(layout));
    });
  }

  onSelect(layout: fromLayout.TLayout) {
    if (!layout.isSelected) {
      this.fromSectionStore.dispatch(new fromSection.ClearSelectedSection());
      this.fromLayoutStore.dispatch(new fromLayout.ClearSelectedLayout());
      this.fromContentStore.dispatch(new fromContent.ClearSelectedContent());
      this.fromLayoutStore.dispatch(new fromLayout.SetSelectedLayout(layout));
    }
  }

  onDelete(layoutId: number) {
    this.fromLayoutStore.dispatch(new fromLayout.DeleteLayout(layoutId));
  }

  addLayout(data): fromLayout.TAddLayout {
    const { layoutId, numberOfColumns } = data.item["new-layout"];
    const sectionId = data.to["section-id"];
    const position = data.newIndex;

    return {
      sectionId: sectionId,
      layoutId: layoutId,
      numberOfColumns: numberOfColumns,
      position: position
    };
  }

  sortLayout(data): fromLayout.TSortLayout {
    const layoutId = data.item["layout-id"];
    const sectionId = data.to["section-id"];

    return {
      sectionId: sectionId,
      layoutIds: this.mapper(this.components),
      movedLayoutId: layoutId || 0
    };
  }

  mapper(items: fromLayout.TLayout[]): number[] {
    return items.map(item => {
      return item.id;
    });
  }
}
