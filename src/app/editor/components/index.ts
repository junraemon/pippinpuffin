import { EmailSectionComponent } from "./email-section/email-section.component";
import { EmailSectionFormComponent } from "./email-section-form/email-section-form.component";

import { EmailLayoutComponent } from "./email-layout/email-layout.component";
import { EmailLayoutFormComponent } from "./email-layout-form/email-layout-form.component";

import { EmailContentComponent } from "./email-content/email-content.component";
import { EmailTextFormComponent } from "./email-text-form/email-text-form.component";
import { EmailButtonFormComponent } from "./email-button-form/email-button-form.component";
import { EmailImageFormComponent } from "./email-image-form/email-image-form.component";
import { EmailContentFormComponent } from "./email-content-form/email-content-form.component";

export const components: any[] = [
  EmailSectionComponent,
  EmailSectionFormComponent,
  EmailLayoutComponent,
  EmailLayoutFormComponent,
  EmailContentComponent,
  EmailContentFormComponent,
  EmailTextFormComponent,
  EmailButtonFormComponent,
  EmailImageFormComponent
];

export * from "./email-section/email-section.component";
export * from "./email-section-form/email-section-form.component";

export * from "./email-layout/email-layout.component";
export * from "./email-layout-form/email-layout-form.component";

export * from "./email-content/email-content.component";
export * from "./email-content-form/email-content-form.component";
export * from "./email-text-form/email-text-form.component";
export * from "./email-button-form/email-button-form.component";
export * from "./email-image-form/email-image-form.component";
