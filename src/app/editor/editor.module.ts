import { CommonModule } from "@angular/common";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSidenavModule,
  MatToolbarModule
} from "@angular/material";
import { FoundationLibModule } from "@lib/foundation";
import { SharedModule } from "@pippin/shared/shared.module";
import { EditorModule } from "@tinymce/tinymce-angular";
import { SortablejsModule } from "angular-sortablejs";
import { MccColorPickerModule } from "material-community-components";
import * as editorRouting from "./editor.routing";

declare var tinyMCE: any;
tinyMCE.baseURL = "assets/tinymce"; // trailing slash important

const MaterialModules: any[] = [
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatButtonModule,
  MatIconModule,
  MatSidenavModule,
  MatToolbarModule,
  MatSelectModule,
  MatCheckboxModule
];

@NgModule({
  schemas: [NO_ERRORS_SCHEMA],
  imports: [
    ...MaterialModules,
    SharedModule,
    CommonModule,
    ReactiveFormsModule,
    FoundationLibModule,
    EditorModule,
    editorRouting.EditorRoutingModule,
    SortablejsModule,
    MccColorPickerModule.forRoot({
      empty_color: "none"
    })
  ],
  entryComponents: [...editorRouting.components],
  declarations: [...editorRouting.containers, ...editorRouting.components]
})
export class PEditorModule {}
