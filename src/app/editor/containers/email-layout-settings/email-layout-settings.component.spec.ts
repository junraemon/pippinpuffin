import { RouterTestingModule } from "@angular/router/testing";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { EmailLayoutSettingsComponent } from "./email-layout-settings.component";
import { MatIconModule, MatToolbarModule } from "@angular/material";
import { EmailLayoutFormComponent } from "@pippin/editor/components";
import { PStoreModule } from "@store/store.module";

describe("EmailLayoutSettingsComponent", () => {
  let component: EmailLayoutSettingsComponent;
  let fixture: ComponentFixture<EmailLayoutSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatIconModule,
        MatToolbarModule,
        PStoreModule.forRoot(),
        RouterTestingModule
      ],
      declarations: [EmailLayoutSettingsComponent, EmailLayoutFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailLayoutSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
