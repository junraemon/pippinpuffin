import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";

import * as fromLayout from "@store/layout";

@Component({
  selector: "pippin-email-layout-settings",
  templateUrl: "./email-layout-settings.component.html",
  styleUrls: ["./email-layout-settings.component.scss"]
})
export class EmailLayoutSettingsComponent implements OnInit {
  selected$: Observable<fromLayout.TLayout>;

  constructor(private store: Store<fromLayout.LayoutState>) {
    this.selected$ = this.store.select(fromLayout.getSelected);
  }

  close() {
    this.store.dispatch(new fromLayout.ClearSelectedLayout());
  }

  ngOnInit() {}
}
