import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { ReactiveFormsModule } from "@angular/forms";
import { MatToolbarModule, MatIconModule } from "@angular/material";

import { EmailContentSettingsComponent } from "./email-content-settings.component";
import { EmailTextFormComponent } from "@pippin/editor/components";

xdescribe("EmailContentFormComponent", () => {
  let component: EmailContentSettingsComponent;
  let fixture: ComponentFixture<EmailContentSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, MatToolbarModule, MatIconModule],
      declarations: [EmailContentSettingsComponent, EmailTextFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailContentSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
