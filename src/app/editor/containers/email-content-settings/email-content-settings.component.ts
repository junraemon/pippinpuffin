import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";

import * as fromContent from "@store/content";

@Component({
  selector: "pippin-email-content-settings",
  templateUrl: "./email-content-settings.component.html",
  styleUrls: ["./email-content-settings.component.scss"]
})
export class EmailContentSettingsComponent {
  selectedContent$: Observable<fromContent.TContent>;
  isContentFormActive = false;

  constructor(private store: Store<fromContent.ContentState>) {
    this.selectedContent$ = this.store.select(fromContent.getSelected);
  }

  close() {
    this.store.dispatch(new fromContent.ClearSelectedContent());
  }

  toggleContentSettings() {
    this.isContentFormActive = !this.isContentFormActive;
  }
}
