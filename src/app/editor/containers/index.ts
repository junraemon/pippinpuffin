import { EmailEditorComponent } from "./email-editor/email-editor.component";
import { EmailEditorSectionComponent } from "./email-editor/email-editor-section.component";
import { EmailEditorLayoutComponent } from "./email-editor/email-editor-layout.component";
import { EmailEditorContentComponent } from "./email-editor/email-editor-content.component";

import { EmailSectionSettingsComponent } from "./email-section-settings/email-section-settings.component";
import { EmailLayoutSettingsComponent } from "./email-layout-settings/email-layout-settings.component";
import { EmailContentSettingsComponent } from "./email-content-settings/email-content-settings.component";

export const containers: any[] = [
  EmailEditorComponent,
  EmailEditorSectionComponent,
  EmailEditorLayoutComponent,
  EmailEditorContentComponent,
  EmailSectionSettingsComponent,
  EmailLayoutSettingsComponent,
  EmailContentSettingsComponent
];

export * from "./email-editor/email-editor.component";
export * from "./email-section-settings/email-section-settings.component";
export * from "./email-layout-settings/email-layout-settings.component";
export * from "./email-content-settings/email-content-settings.component";
