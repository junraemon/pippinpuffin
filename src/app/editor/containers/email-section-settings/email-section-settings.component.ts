import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";

import * as fromSection from "@store/section";

@Component({
  selector: "pippin-email-section-settings",
  templateUrl: "./email-section-settings.component.html",
  styleUrls: ["./email-section-settings.component.scss"]
})
export class EmailSectionSettingsComponent implements OnInit {
  selected$: Observable<fromSection.TSection>;

  constructor(private store: Store<fromSection.SectionState>) {
    this.selected$ = this.store.select(fromSection.getSelected);
  }

  close() {
    this.store.dispatch(new fromSection.ClearSelectedSection());
  }

  ngOnInit() {}
}
