import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { EmailSectionSettingsComponent } from "./email-section-settings.component";
import { MatIconModule, MatToolbarModule } from "@angular/material";
import { PStoreModule } from "@store/store.module";
import { RouterTestingModule } from "@angular/router/testing";
import { EmailSectionFormComponent } from "@pippin/editor/components";

describe("EmailSectionSettingsComponent", () => {
  let component: EmailSectionSettingsComponent;
  let fixture: ComponentFixture<EmailSectionSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatIconModule,
        MatToolbarModule,
        PStoreModule.forRoot(),
        RouterTestingModule
      ],
      declarations: [EmailSectionSettingsComponent, EmailSectionFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailSectionSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
