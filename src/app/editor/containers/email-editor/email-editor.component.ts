import {
  Component,
  OnInit,
  ViewChild,
  ComponentRef,
  ElementRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  OnDestroy
} from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { Store } from "@ngrx/store";

import * as fromTemplate from "@store/template";

import { EmailSectionComponent } from "@pippin/editor/components/email-section/email-section.component";
import { LoaderService } from "@pippin/core";

@Component({
  selector: "pippin-email-editor",
  templateUrl: "./email-editor.component.html",
  styleUrls: ["./email-editor.component.scss"]
})
export class EmailEditorComponent implements OnInit, OnDestroy {
  template$: Observable<fromTemplate.Template>;
  hasSelectedElement$: Observable<boolean>;
  templateId: number;
  trustedUrl: SafeResourceUrl;

  @ViewChild("iframe")
  iframe: ElementRef;
  doc: any;
  compRef: ComponentRef<EmailSectionComponent>;

  constructor(
    private fromTemplateStore: Store<fromTemplate.TemplateState>,
    private sanitizer: DomSanitizer,
    private vcRef: ViewContainerRef,
    private resolver: ComponentFactoryResolver,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.template$ = this.fromTemplateStore
      .select(fromTemplate.getSelectedTemplate)
      .pipe(
        tap((template: fromTemplate.Template) => {
          if (template) {
            this.loaderService.show();
            this.templateId = template.id;
            this.trustedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
              `/TemplatePage/Edit/${template.id}`
            );
          }
        })
      );

    this.hasSelectedElement$ = this.fromTemplateStore.select(
      fromTemplate.hasSelectedElement
    );
  }

  createComponent() {
    const compFactory = this.resolver.resolveComponentFactory(
      EmailSectionComponent
    );

    this.compRef = this.vcRef.createComponent(compFactory);

    this.compRef.instance.templateId = this.templateId;

    this.doc
      .getElementById("section-container")
      .appendChild(this.compRef.location.nativeElement);
  }

  onLoad() {
    if (!!this.iframe) {
      this.doc =
        this.iframe.nativeElement.contentDocument ||
        this.iframe.nativeElement.contentWindow;
      this.createComponent();
      this.loaderService.hide();
    }
  }

  ngOnDestroy() {
    if (this.compRef) {
      this.compRef.destroy();
    }
  }
}
