import { Component } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { SortablejsOptions } from "angular-sortablejs";

@Component({
  selector: "pippin-email-editor-layout",
  template: `
    <div *ngIf="(layouts$ | async) as layouts">
      <mat-list role="list" [sortablejs]="layouts" [sortablejsOptions]="layoutSortableOptions">
        <h3 mat-subheader>
          <mat-icon mat-list-icon>view_module</mat-icon> &nbsp;
          Layouts
        </h3>  
        <mat-list-item *ngFor="let layout of layouts" [new-layout]="layout" class="sortable-item sortable-item-handler" role="listitem">
          <mat-icon mat-list-icon>drag_handle</mat-icon>
          <h4 mat-line> {{ layout.name }} </h4>
        </mat-list-item>
      </mat-list>
    </div>
    `
})
export class EmailEditorLayoutComponent {
  layouts$: BehaviorSubject<any[]> = new BehaviorSubject(null);

  layoutSortableOptions: SortablejsOptions = {
    sort: false,
    group: {
      name: "layouts",
      pull: "clone",
      put: false
    }
  };

  constructor() {
    this.layouts$.next([
      {
        id: 1,
        name: "1 Columned Layout",
        numberOfColumns: 1
      },
      {
        id: 2,
        name: "2 Columned Layout(6x6)",
        numberOfColumns: 2
      },
      {
        id: 3,
        name: "3 Columned Layout(4x4x4)",
        numberOfColumns: 3
      },
      {
        id: 4,
        name: "4 Columned Layout(3x3x3x3)",
        numberOfColumns: 4
      }
    ]);
  }
}
