import { Component } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { SortablejsOptions } from "angular-sortablejs";

import { ContentType } from "@store/content";

@Component({
  selector: "pippin-email-editor-content",
  template: `
    <div *ngIf="(contents$ | async) as contents">
      <mat-list role="list" [sortablejs]="contents" [sortablejsOptions]="contentSortableOptions">
        
      <h3 mat-subheader>
          <mat-icon mat-list-icon>dashboard</mat-icon> &nbsp;
          Contents
        </h3>  

        <mat-list-item 
        *ngFor="let content of contents" 
        [newContent]="content.payload" 
        class="sortable-item sortable-item-handler" 
        role="listitem">

          <mat-icon mat-list-icon>drag_handle</mat-icon>
          <h4 mat-line> {{ content.name }} </h4>
          
        </mat-list-item>
      </mat-list>

    <div>
  `
})
export class EmailEditorContentComponent {
  contents$: BehaviorSubject<any[]> = new BehaviorSubject(null);

  contentSortableOptions: SortablejsOptions = {
    sort: false,
    group: {
      name: "contents",
      pull: "clone",
      put: false
    }
  };

  constructor() {
    this.contents$.next([
      {
        name: "Text",
        payload: {
          type: ContentType.Text,
          typeId: 0
        }
      },
      {
        name: "Button",
        payload: {
          type: ContentType.Button,
          typeId: 0
        }
      },
      {
        name: "Image",
        payload: {
          type: ContentType.Image,
          typeId: 0
        }
      }
    ]);
  }
}
