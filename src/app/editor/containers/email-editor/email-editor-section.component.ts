import { Component } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { SortablejsOptions } from "angular-sortablejs";

@Component({
  selector: "pippin-email-editor-section",
  template: `
    <div *ngIf="(sections$ | async) as sections">
      <mat-list role="list" [sortablejs]="sections" [sortablejsOptions]="sectionSortableOptions">
        
        <h3 mat-subheader>
          <mat-icon mat-list-icon>view_streams</mat-icon> &nbsp;
          Sections
        </h3>

        <mat-list-item  
        *ngFor="let section of sections" 
        [new-section-id]="section.id" 
        class="sortable-item sortable-item-handler" 
        role="listitem">
         
          <mat-icon mat-list-icon>drag_handle</mat-icon>
          <h4 mat-line> {{ section.name }} </h4>

        </mat-list-item>
      </mat-list>

    </div>
    `
})
export class EmailEditorSectionComponent {
  sections$: BehaviorSubject<any[]> = new BehaviorSubject(null);

  sectionSortableOptions: SortablejsOptions = {
    sort: false,
    group: {
      name: "sections",
      pull: "clone",
      put: false
    }
  };

  constructor() {
    this.sections$.next([
      {
        id: 1,
        name: "Empty Section"
      }
    ]);
  }
}
