import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// containers
import * as fromContainers from "./containers";

// components
import * as fromComponents from "./components";

// guards and services
import * as fromThemeGuards from "@store/theme/guards";
import * as fromTemplateGuards from "@store/template/guards";
import * as fromSectionGuards from "@store/section/guards";

const routes: Routes = [
  {
    path: "email/:themeId/:templateId",
    canActivate: [
      fromThemeGuards.ThemeExistsGuards,
      fromTemplateGuards.TemplateExistsGuards,
      fromSectionGuards.SectionsGuard
    ],
    component: fromContainers.EmailEditorComponent
  },
  { path: "**", redirectTo: "/" }
];

export const EditorRoutingModule: ModuleWithProviders = RouterModule.forChild(
  routes
);

export * from "./containers";
export * from "./components";
