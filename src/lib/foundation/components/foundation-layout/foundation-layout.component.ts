import {
  Component,
  OnInit,
  Input,
  ContentChild,
  TemplateRef
} from "@angular/core";

@Component({
  /* tslint:disable */
  selector: "foundation-layout",
  templateUrl: "./foundation-layout.component.html",
  styleUrls: ["./foundation-layout.component.scss"]
})
export class FoundationLayoutComponent implements OnInit {
  @Input() layout: any;
  @ContentChild("columnRef") columnRef: TemplateRef<any>;

  constructor() {}

  ngOnInit(): void {}
}
