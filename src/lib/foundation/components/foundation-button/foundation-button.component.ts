import { Component, OnInit, Input } from "@angular/core";

export interface FoundationButton {
  link: string;
  caption: string;
  alignment: string;
  size: string;
  type: string;
  corners: string;
  expanded: string;
}

@Component({
  /* tslint:disable */
  selector: "foundation-button",
  templateUrl: "./foundation-button.component.html",
  styleUrls: ["./foundation-button.component.scss"]
})
export class FoundationButtonComponent implements OnInit {
  @Input() data: FoundationButton;
  constructor() {}

  ngOnInit(): void {}

  get combineClass() {
    return [
      this.data.size,
      this.data.type,
      this.data.corners,
      this.data.expanded
    ].join(" ");
  }
}
