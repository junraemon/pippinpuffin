import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { FoundationSectionComponent } from "./foundation-section.component";

describe("FoundationSectionComponent", () => {
  let component: FoundationSectionComponent;
  let fixture: ComponentFixture<FoundationSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FoundationSectionComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundationSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
