import { Component, OnInit, Input } from "@angular/core";

@Component({
  /* tslint:disable */
  selector: "foundation-section",
  templateUrl: "./foundation-section.component.html",
  styleUrls: ["./foundation-section.component.scss"]
})
export class FoundationSectionComponent implements OnInit {
  @Input() classes: string;
  constructor() {}

  ngOnInit() {}
}
