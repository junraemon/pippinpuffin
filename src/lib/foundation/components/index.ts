import { FoundationButtonComponent } from "./foundation-button/foundation-button.component";
import { FoundationContentComponent } from "./foundation-content/foundation-content.component";
import { FoundationImageComponent } from "./foundation-image/foundation-image.component";
import { FoundationLayoutComponent } from "./foundation-layout/foundation-layout.component";
import { FoundationSectionComponent } from "./foundation-section/foundation-section.component";
import { FoundationTextComponent } from "./foundation-text/foundation-text.component";
import { FoundationCalloutComponent } from "./foundation-callout/foundation-callout.component";

export const components: any[] = [
  FoundationSectionComponent,
  FoundationLayoutComponent,
  FoundationContentComponent,
  FoundationTextComponent,
  FoundationImageComponent,
  FoundationButtonComponent,
  FoundationCalloutComponent
];

export * from "./foundation-button/foundation-button.component";
export * from "./foundation-image/foundation-image.component";
export * from "./foundation-layout/foundation-layout.component";
export * from "./foundation-section/foundation-section.component";
export * from "./foundation-text/foundation-text.component";
export * from "./foundation-callout/foundation-callout.component";
