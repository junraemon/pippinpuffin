import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { FoundationCalloutComponent } from "./foundation-callout.component";

describe("FoundationCalloutComponent", () => {
  let component: FoundationCalloutComponent;
  let fixture: ComponentFixture<FoundationCalloutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FoundationCalloutComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundationCalloutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
