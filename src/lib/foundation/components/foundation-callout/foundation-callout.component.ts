import { Component, OnInit, Input } from "@angular/core";

@Component({
  /* tslint:disable */
  selector: "foundation-callout",
  templateUrl: "./foundation-callout.component.html",
  styleUrls: ["./foundation-callout.component.scss"]
})
export class FoundationCalloutComponent implements OnInit {
  @Input()
  calloutStyle: string;

  types = [
    "",
    "default",
    "primary",
    "secondary",
    "success",
    "warning",
    "alert",
    "primary"
  ];
  constructor() {}

  ngOnInit() {}

  get typeInString() {
    return this.types[this.calloutStyle];
  }

  get isCallout() {
    return parseInt(this.calloutStyle) !== 0;
  }
}
