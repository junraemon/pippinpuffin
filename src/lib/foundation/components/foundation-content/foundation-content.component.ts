import { Component, OnInit, Input } from "@angular/core";

export interface FoundationContent {
  backgroundColor: string;
  spacing: number;
  customClass: string;
  calloutStyle: number;
}
@Component({
  /* tslint:disable */
  selector: "foundation-content",
  templateUrl: "./foundation-content.component.html",
  styleUrls: ["./foundation-content.component.scss"]
})
export class FoundationContentComponent implements OnInit {
  @Input()
  content: FoundationContent;

  constructor() {}

  ngOnInit() {}
}
