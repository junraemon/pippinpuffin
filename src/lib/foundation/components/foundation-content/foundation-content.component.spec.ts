import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { FoundationContentComponent } from "./foundation-content.component";
import { FoundationCalloutComponent } from "./../foundation-callout/foundation-callout.component";

describe("FoundationContentComponent", () => {
  let component: FoundationContentComponent;
  let fixture: ComponentFixture<FoundationContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FoundationContentComponent, FoundationCalloutComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundationContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
