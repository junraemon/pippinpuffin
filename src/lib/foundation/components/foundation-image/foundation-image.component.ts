import { Component, OnInit, Input } from "@angular/core";

export interface FoundationImage {
  url: string;
  alignment: string;
  width: number;
  height: number;
}

@Component({
  /* tslint:disable */
  selector: "foundation-image",
  templateUrl: "./foundation-image.component.html",
  styleUrls: ["./foundation-image.component.scss"]
})
export class FoundationImageComponent implements OnInit {
  @Input() data: FoundationImage;

  placeholder = "https://placehold.it/600x200?text=small-center";
  constructor() {}

  ngOnInit(): void {}

  get url() {
    return !!this.data.url &&
      this.data.url.match(/\.(jpeg|jpg|gif|png)$/) != null
      ? this.data.url
      : this.placeholder;
  }

  get width() {
    return !!this.data.width ? this.data.width : null;
  }

  get height() {
    return !!this.data.height ? this.data.height : null;
  }

  get alignment() {
    return !!this.data.alignment ? "float-" + this.data.alignment : null;
  }
}
