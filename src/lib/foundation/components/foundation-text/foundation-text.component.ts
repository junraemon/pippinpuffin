import { Component, OnInit, Input } from "@angular/core";
import { SafeHtml } from "@angular/platform-browser";

@Component({
  /* tslint:disable */
  selector: "foundation-text",
  templateUrl: "./foundation-text.component.html",
  styleUrls: ["./foundation-text.component.scss"]
})
export class FoundationTextComponent implements OnInit {
  @Input() content: SafeHtml;
  constructor() {}

  ngOnInit(): void {}
}
