using AutoMapper;
using PippinPuffin.Controllers.Resources;
using PippinPuffin.Core.Models;

namespace PippinPuffin.Mapping
{
    public class MappingProfile : Profile
    {

        public MappingProfile()
        {
            // Domain to API Resource
            CreateMap<Theme, KeyValuePairResource>();
            CreateMap<Theme, ViewThemesResource>();
            CreateMap<Theme, ViewThemeResource>()
                .ForMember(tr => tr.Templates, opt => opt.MapFrom(t => t.Templates));
            CreateMap<Template, TemplateResource>()
                .ForMember(tr => tr.ThemeId, opt => opt.MapFrom(t => t.ThemeId));
            CreateMap<ComponentSection, ComponentSectionResource>();
            CreateMap<ComponentLayout, ComponentLayoutResource>()
                .ForMember(lr => lr.ComponentColumns, opt => opt.MapFrom(l => l.ComponentColumns));
            CreateMap<ComponentColumn, ComponentColumnResource>();
            CreateMap<ComponentContent, ComponentContentResource>();


            // API Resource to Domain
            CreateMap<KeyValuePairResource, Theme>()
                .ForMember(v => v.Id, opt => opt.Ignore());
            CreateMap<SaveThemeResource, Theme>();
            CreateMap<SaveTemplateResource, Template>()
                .ForMember(v => v.Id, opt => opt.Ignore());
            CreateMap<ComponentSectionResource, ComponentSection>()
                .ForMember(v => v.Id, opt => opt.Ignore());
            CreateMap<ComponentLayoutResource, ComponentLayout>()
                .ForMember(v => v.Id, opt => opt.Ignore());
            CreateMap<ComponentColumnResource, ComponentColumn>()
                .ForMember(v => v.Id, opt => opt.Ignore());
            CreateMap<ComponentContentResource, ComponentContent>()
                .ForMember(v => v.Id, opt => opt.Ignore());
            CreateMap<SaveComponentContentResource, ComponentContent>()
                .ForMember(v => v.Id, opt => opt.Ignore());
            CreateMap<ContentTextResource, ContentText>()
                .ForMember(v => v.Id, opt => opt.Ignore());
            CreateMap<ContentButtonResource, ContentButton>()
            .ForMember(v => v.Id, opt => opt.Ignore());
            CreateMap<ContentImageResource, ContentImage>()
            .ForMember(v => v.Id, opt => opt.Ignore());
        }
    }
}
