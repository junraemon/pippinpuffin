﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PippinPuffin.Migrations
{
    public partial class UpdateFieldsToComponentContent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BackgroundColor",
                table: "ComponentContents",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CalloutStyle",
                table: "ComponentContents",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "CustomClass",
                table: "ComponentContents",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Spacing",
                table: "ComponentContents",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BackgroundColor",
                table: "ComponentContents");

            migrationBuilder.DropColumn(
                name: "CalloutStyle",
                table: "ComponentContents");

            migrationBuilder.DropColumn(
                name: "CustomClass",
                table: "ComponentContents");

            migrationBuilder.DropColumn(
                name: "Spacing",
                table: "ComponentContents");
        }
    }
}
