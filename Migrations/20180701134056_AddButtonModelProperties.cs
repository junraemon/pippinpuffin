﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PippinPuffin.Migrations
{
    public partial class AddButtonModelProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Corners",
                table: "ContentButtons",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Expanded",
                table: "ContentButtons",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "ContentButtons",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Corners",
                table: "ContentButtons");

            migrationBuilder.DropColumn(
                name: "Expanded",
                table: "ContentButtons");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "ContentButtons");
        }
    }
}
