﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PippinPuffin.Migrations
{
    public partial class AddTemplateModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Templates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    ThemeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Templates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Templates_Themes_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Themes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ComponentSections",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    Position = table.Column<int>(nullable: false),
                    TemplateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComponentSections", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComponentSections_Templates_TemplateId",
                        column: x => x.TemplateId,
                        principalTable: "Templates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ComponentLayouts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ComponentSectionId = table.Column<int>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    Position = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComponentLayouts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComponentLayouts_ComponentSections_ComponentSectionId",
                        column: x => x.ComponentSectionId,
                        principalTable: "ComponentSections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ComponentColumns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ColumnSizes = table.Column<string>(nullable: true),
                    ComponentLayoutId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComponentColumns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComponentColumns_ComponentLayouts_ComponentLayoutId",
                        column: x => x.ComponentLayoutId,
                        principalTable: "ComponentLayouts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ComponentContents",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ComponentColumnId = table.Column<int>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    Position = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComponentContents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComponentContents_ComponentColumns_ComponentColumnId",
                        column: x => x.ComponentColumnId,
                        principalTable: "ComponentColumns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ComponentColumns_ComponentLayoutId",
                table: "ComponentColumns",
                column: "ComponentLayoutId");

            migrationBuilder.CreateIndex(
                name: "IX_ComponentContents_ComponentColumnId",
                table: "ComponentContents",
                column: "ComponentColumnId");

            migrationBuilder.CreateIndex(
                name: "IX_ComponentLayouts_ComponentSectionId",
                table: "ComponentLayouts",
                column: "ComponentSectionId");

            migrationBuilder.CreateIndex(
                name: "IX_ComponentSections_TemplateId",
                table: "ComponentSections",
                column: "TemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_Templates_ThemeId",
                table: "Templates",
                column: "ThemeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ComponentContents");

            migrationBuilder.DropTable(
                name: "ComponentColumns");

            migrationBuilder.DropTable(
                name: "ComponentLayouts");

            migrationBuilder.DropTable(
                name: "ComponentSections");

            migrationBuilder.DropTable(
                name: "Templates");
        }
    }
}
