﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PippinPuffin.Migrations
{
    public partial class AddImageModelProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Alignment",
                table: "ContentImages",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Height",
                table: "ContentImages",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "ContentImages",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Width",
                table: "ContentImages",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Alignment",
                table: "ContentImages");

            migrationBuilder.DropColumn(
                name: "Height",
                table: "ContentImages");

            migrationBuilder.DropColumn(
                name: "Url",
                table: "ContentImages");

            migrationBuilder.DropColumn(
                name: "Width",
                table: "ContentImages");
        }
    }
}
