﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PippinPuffin.Migrations
{
    public partial class AddButtonModelPropertiesCaptionAlignment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Alignment",
                table: "ContentButtons",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Caption",
                table: "ContentButtons",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Alignment",
                table: "ContentButtons");

            migrationBuilder.DropColumn(
                name: "Caption",
                table: "ContentButtons");
        }
    }
}
